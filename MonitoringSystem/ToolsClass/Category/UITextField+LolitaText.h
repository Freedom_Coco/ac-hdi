//
//  UITextField+LolitaText.h
//  DataStatistics
//
//  Created by Kang on 16/3/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UITextFieldLolitaDelegate <NSObject>

- (void)addYTYanKTextFielLengthMethod:(nonnull UITextField *)field;
@end

@interface UITextField (LolitaText)
@property (assign, nonatomic) _Nullable id <UITextFieldLolitaDelegate> lolitaDelegate;

- (void)addObserverForWillKeyBoard:(nullable id)obj;
- (void)removeObserverForKeyBoard:(nonnull id)obj;


- (void)addYTYanKToolBarView:(nonnull UIToolbar *)toolView withItem:(nullable UIButton *)itemBtn;
- (void)addYTYanKTextFielStyle1:(nonnull UITextField *)text withString:(nullable NSString *)str;
//- (void)addYTYanKTextFielStyle2:(nonnull UITextField *)text withString:(nonnull NSString *)str;

- (void)addEditChangedNotificationWithObserver:(nonnull UITextField *)field;

@end
