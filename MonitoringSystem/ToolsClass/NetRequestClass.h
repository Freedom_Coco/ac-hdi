//
//  NetRequestClss.h
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//


/**
 *
    ************************* 请求类 - 负责 Get请求\ POST 请求\ 监测网络连接情况.全项目中的所有请求都在此类中实现。
                              改日要更换请求基础库或者升级请求库文件，只需要修改此类   
 
 GET请求基方法
 1、   + (void) GetDataForRequestAndBlockResult:(NSString *) requestUrl andRequestParam:(NSDictionary *)param andResult:(void(^)(id successResult)) success andFailureBlock:(void(^)(NSString * failureResult)) failure
 
 POST请求基方法
 2、   + (void) PostDataForRequestAndBlockResult:(NSString *) requestUrl andRequestParam:(NSDictionary *)param andUpload:(BOOL)upload andResult:(void(^)(id successResult)) success andFailureBlock:(void(^)(NSString * failureResult)) failure
 
    *************************
 
 */

/** 获取到数据的情况 */
typedef NS_ENUM(NSInteger, NetObtainDataStatus) {
    NetObtainDataStatusUnknownAlsoFail      = -2,  // 返回了登陆 -失败
    NetObtainDataStatusFail                 = -1,  // 失败
    NetObtainDataStatusError                = 0,   // error
    NetObtainDataStatusSuccess              = 1,   // 成功
    NetObtainDataStatusSuccessAlsoNotData   = 2,   // 成功 -无数据
};

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "ACUserLoginManage.h"
@class ACQRDeviceModel;
@class ACDeviceListModelCenter;
@class PPQueryGpsModel;
@class ACDeviceListModel;
@class ACDeviceDetailModel;
@class ACDeviceInfoModel;
@interface NetRequestClass : NSObject

#pragma mark *********************注册、登录、忘记密码界面的所有的接口在这里*********************

/**
 *   校验手机号码是否已经存在在后台，0异常，1，未注册，2，已注册
 *
 *  @param obj     发起请求的所在的类
 *  @param url     url
 *  @param param   loginName手机号码，zone区号
 *  @param success 成功回调
 *  @param failure 失败回调
 */
+ (void ) requestWithUrlForCheckPhoneNumber:(id) obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(NSInteger result))success failure:(void(^)(NSString * error))failure;


/**
 *   校验手机的验证码是否是对的  0,异常，校验失败，1，校验成功
 *
 *  @param obj     发起请求的所在的类
 *  @param param   loginName账号，zone区号，verityCode校验码
 
 */
+ (void) requestWithUrlForCheckPhoneMessages:(id) obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(NSInteger result))success failure:(void(^)(NSString * error))failure;

/**
 *   用户登录
 
 0：用户名密码为空
 1：用户不存在
 2：密码错误
 3：服务器忙
 4：验证码错误
 5：没有权限
 6：停用账号
 8：登录成功
 */
+ (void) requestWithUrlForLoginIn:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(ACUserLoginManage * model))success failure:(void(^)(NSString * error))failure;

/**
 *  设置密码       -------- 在用户注册界面
 0：失败
 1：成功
 
 *
 *
 */
+ (void) requestWithUrlForSetPwd:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void (^)(NSInteger result))success failure:(void (^)(NSString *))failure;


/**
 *  设置新的密码       -------- 在用户忘记密码界面
 0：失败
 1：成功
 *
 *
 */
+ (void) requestWithUrlForForgetPwd:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void (^)(NSInteger result))success failure:(void (^)(NSString *))failure;


/**
 *   退出操作
 *
 */
+ (void) requestWithUrlForLogOut:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void (^)(BOOL result))success failure:(void (^)(NSString *))failure;



#pragma mark *********************个人用户界面的所有的接口在这里*********************

/**
 *   修改用户密码     ---------用户修改自己的密码
 *
 0：禁止修改
 1：密码错误
 2：修改成功
 3：session超时
 4：账号不一致
 
 */
+ (void) requestWithUrlChangeUserPwd:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(NSString * codeStr))success failure:(void(^)(NSString * error))failure;


/**
 *   修改用户信息。修改上传头像，修改用户名 ------------ 在用户界面
 0：失败
 1：成功

 *
 */
+ (void) requestWithUrlChangeUserInfo:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void (^)(NSString *result,NSInteger tag))success failure:(void (^)(NSString *))failure;




#pragma mark *********************  设备信息的所有的接口在这里 *********************
/**
 *  二维码扫描，显示设备的相信信息
 *
 */
+ (void) requestWithUrlForDeviceDetail:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void (^)(ACQRDeviceModel *result))success failure:(void (^)(NSString *))failure;

/**
 *  二维码扫描出设备后，用户add绑定设备

 1：成功
 2:已绑定
 3：空间不足
 4：没绑定过，不能重绑（仅使用deviceNo时返回才会返回）

 */
+ (void) requestWithUrlForBindingDevice:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void (^)(NSInteger result))success failure:(void (^)(NSString *))failure;

/**
 *  获取 设备信息-- 绑定成功后，在设备详情界面进入，用于修改设备信息

 */
+ (void) requestWithUrlForGetDeviceMessage:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void (^)(ACDeviceDetailModel *result))success failure:(void (^)(NSString *))failure;


/**
 *
 *  修改设备信息
 */
+ (void) requestWithUrlForUpdateDeviceInfo:(id) obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(NSInteger result))success failure:(void(^)(NSString *))failure;
#pragma mark *********************  设备列表的所有的接口在这里 *********************
/**
 *  解除设备的绑定
 0：失败
 1：成功
 2：设备未绑定

 */
+ (void) requestWithUrlForUnBindingDevice:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void (^)(NSInteger result))success failure:(void (^)(NSString *))failure;



/**
 *  获取设备列表  搜索设备信息.数组形式接受.注意：搜索跟获取设备列表，同是采用这个接口
 current_page           当前页索引       int      M	当前加载的页数
 mapVo['deviceCode']	搜索条件        string	M	搜索条件，按照单引号当中填充 的字段名称，进行条件搜索，如：mapVo['deviceCode']，则按设备编号进行搜索
 ascendingOrder         排序状态        string	M	1、升序。2、降序。《默认状态为降序》
 orderName              条件排序状态     string	M	根据orderName来判断是否跟最近使用时间还是总流量排序,对应的参数值：orderName=remain。orderName=latestUse
NSInteger type   0 为全局  1为模糊
 */
+ (void) requestWithUrlForDviceList:(id)obj andUrl:(NSString *)url andParam:(NSString *)deviceCode andAscendingOrder:(NSString *)ascendingOrder andOrderName:(NSString *)orderName andPage:(NSInteger )current_page andState:(NSInteger)state success:(void (^)(NSDictionary *result))success failure:(void (^)(NSString *))failure;


/**
 *  获取设备 -------------- 根据对应的获取对应的状态获取对应的设备信息
 *
 */
+ (void)requestWithUrlForDeviceWithDeviceState:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param andPage:(NSInteger )current_page andState:(NSInteger)state success:(void (^)(NSArray *result))success failure:(void (^)(NSString *))failure;




#pragma mark ********************* 设备详情的接口都在此处     *********************

/**
 *  设备详情  打油界面
 *
 */
+ (void) requestWithUrlForGetDeviceInfo:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(ACDeviceInfoModel *result))success failure:(void (^)(NSString *))failure;


/**
 *   加油接口
 *
 */
+ (void) requestWithUrlForAddOil:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(NSDictionary* result))success failure:(void(^)(NSString *))failure;


/**
 *  权限转移
 *
 */
+ (void) requestWithUrlForTranistAuthor:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(NSInteger result))success failure:(void(^)(NSString *))failure;

#pragma mark *********************  地图。所有的GPS的接口都在此处     *********************

/**
 *   获取单台设备的GPS。用于单台设备的GPS定位

    请求成功后，在对应的数据流返回到到控制层。
 */
+ (void)requestWithURLForQueryDeviceGPS:(id) obj andUrl:(NSString * )url andParam:(NSDictionary *)param success:(void(^)(PPQueryGpsModel* result))success failure:(void (^)(NSString *))failure;


/**
 *
 *  自定义工作时间
 */
+ (void) requestWithUrlForSetWorkingTime:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(NSInteger result))success failure:(void(^)(NSString *))failure;


#pragma mark *********************  泵芯的配置列表     *********************
/**
 *   从服务器中拿取默认的AC泵芯的配置. 推荐配置、正常的配置都是从此处来获取
 isFavoConfig   是否是推荐配置方案（注意：预留字段）
 
 当只传入i_imei参数时，进入查看设备配置数据接口，此时不能不传入state与configPk两个参数。
 当传入state与configPk两个参数时，进入推荐配置列表查看配置接口，此时不能传入i_imei参数
 *
 */
+ (void) requestWithUrlForGetACConfig:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param andiSFavoConfig:(BOOL) isFavoConfig success:(void (^)(NSDictionary *result))success failure:(void (^)(NSString *))failure;

/**
 *   从本地上传配置文件到服务器。
 */
+ (void) requestWithURLForUploadACConfig:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param  success:(void (^)(NSInteger result))success failure:(void (^)(NSString *))failure;


/**
 *  获取推荐列表
 *
 */
+ (void) requestWithUrlForGetDefaultConfig:(id) obj andUrl:(NSString *)url andParam:(NSDictionary *)param success:(void(^)(NSDictionary * result))success failure:(void(^)(NSString *))failure;

/**
 *  删除推荐配置
 */
+ (void) requestWithURLForDelegateRecommdConfig:(id)obj andUrl:(NSString *)url andParam:(NSDictionary *)param  success:(void (^)(NSInteger result))success failure:(void (^)(NSString *))failure;


#pragma mark *********************  软件更新    *********************
+ (void) requestWithUrlForUpdateApp:(NSString *)url success:(void(^)(NSDictionary* versionInfo))success failure:(void(^)(NSString *))failure;

@end
