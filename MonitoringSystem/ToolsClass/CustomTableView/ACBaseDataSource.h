//
//  ACBaseDataSource.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/28.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

/*
  自定义tableView的使用，让tableView从Controller中抽离出来。减少耦合度
 
  使用说明：此类是父类，只要实例化此类，传入一个数据数组，该数组包括了组数据模型。然后设定cell选中的block回调即可。
          注意：传入的数组模型，可视情况而定去解析数据。具体的时候过程看ACDeviceController
 
 */
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class ACDataEntity;
typedef void(^ACTableViewCellClick)(NSInteger obj,NSString * imei);
@interface ACBaseDataSource : NSObject<UITableViewDelegate,UITableViewDataSource>

/**
 *  cell选中后的Block回调
 */
@property (nonatomic,copy) ACTableViewCellClick didSelectCellClick;
/**
 *  数据容器
 */
@property (nonatomic,strong) NSMutableArray * dataModelArray;

/**
 *
 *  构造便利器
 */
+ (instancetype) dataSourceWithEntity:(ACDataEntity *)entity;
- (instancetype) initWithEntity:(ACDataEntity *)entity;

+ (instancetype) dataSourceWithEntity:(ACDataEntity *)entity andWithDeviceState:(NSInteger )state;
- (instancetype) initWithEntity:(ACDataEntity *)entity andWithDeviceState:(NSInteger )state;

@end
