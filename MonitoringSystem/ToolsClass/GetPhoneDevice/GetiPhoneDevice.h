//
//  GetiPhoneDevice.h
//  DataStatistics
//
//  Created by Kang on 15/7/15.
//  Copyright (c) 2015年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GetiPhoneDevice;
@protocol GetiPhoneDeviceDelegate <NSObject>
- (NSString *)CurrentDevice:(NSString *)str;
@end

@interface GetiPhoneDevice : NSObject
@property (assign, nonatomic) id<GetiPhoneDeviceDelegate>delegate;

@property (assign, nonatomic) NSString *iPhoneDevice;
- (void)GetiPhoneDevice;
+ (NSString *)getCurrentDeviceModel;


@end
