//
//  ACCustomButton.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

/**
 *  此类为自定义按钮，主要实现了按钮中的图片的位置，按钮默认是左图右文的，自定义重写父类的方法即可自定义位置
 */
#import <UIKit/UIKit.h>

@interface ACCustomButton : UIButton

@end
