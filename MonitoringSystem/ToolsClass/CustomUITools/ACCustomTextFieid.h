//
//  ACCustomTextFieid.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/20.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

/**
 *       如果继承了此类ACCustomTextFieid，那么在textField输入定位光标时，就可以自定义位置
 */
#import <UIKit/UIKit.h>

@interface ACCustomTextFieid : UITextField

@end
