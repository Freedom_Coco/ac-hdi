
//
//  ACWeakTimer.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/1/16.
//  Copyright © 2016 Interlube. All rights reserved.
//
#import <Foundation/Foundation.h>

typedef void (^HWTimerHandler)(id userInfo);

@interface HWWeakTimer : NSObject

+ (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)interval
                                      target:(id)aTarget
                                    selector:(SEL)aSelector
                                    userInfo:(id)userInfo
                                     repeats:(BOOL)repeats;

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval
                                      block:(HWTimerHandler)block
                                   userInfo:(id)userInfo
                                    repeats:(BOOL)repeats;

@end
