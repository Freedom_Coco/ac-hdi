//
//  UtilToolsClss.m
//  DataStatistics
//
//  Created by Kang on 16/2/4.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "UtilToolsClass.h"
#import "NSDate+TimeAgo.h"
#import "MBProgressHUD+MJ.h"
@interface UtilToolsClass ()<MBProgressHUDDelegate>{
    UIView *BGwinodwView;
    MBProgressHUD *progressHUD;
    
}
@end

@implementation UtilToolsClass
static UtilToolsClass  *utilTool = nil;
+ (UtilToolsClass *)getUtilTools {
    if (!utilTool) {
        utilTool = [[super allocWithZone:NULL] init];
    }
    return utilTool;
}

- (UIButton *)noDataBtn {
    if (!_noDataBtn) {
        _noDataBtn   = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4 , SCREEN_HEIGHT/2 - (SCREEN_WIDTH/2), SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
        _noDataBtn.userInteractionEnabled = NO;
        [_noDataBtn setTitle:NSLocalizedString(@"No Results", "没有结果") forState:UIControlStateNormal];
        [_noDataBtn setTitleColor:RGB(140, 140, 140, 1) forState:UIControlStateNormal];
        _noDataBtn.layer.cornerRadius  = SCREEN_WIDTH/4;
        _noDataBtn.layer.masksToBounds   = YES;
        [_noDataBtn setBackgroundColor:RGB(125, 125, 125, 0.1)];
        
    }
    return _noDataBtn;
}

+ (NSString*)addingPercentEscapesUsingEncodingWithurlStr:(NSString*)urlStr {
    NSString* newUrlStr = @"";
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        newUrlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    else {
        newUrlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    return newUrlStr;
}
+ (id)handleWithNullVlue:(id)vlue {
    __typeof__ (vlue) _vlue = vlue;
    //if ([_vlue isEqual:[NSString class]]) {
    if([_vlue isEqualToString:@"(null)"] || [_vlue isEqualToString:@""]) {
          _vlue = [NSString stringWithFormat:@"---"];
    }
   
    if ( [_vlue isEqual:[NSNull null]]) {
        if ( [_vlue isEqual: [NSNumber class]]) {
            _vlue = @0;
        }else {
            _vlue = [NSString stringWithFormat:@"---"];
        }
    }
   
    return _vlue;

}

/// 增加加载
- (void)addDoLoading {
    [self removeDoLoading];
    UIWindow *temp = [[UIApplication sharedApplication] keyWindow];
    BGwinodwView = [[UIView alloc] init];
    BGwinodwView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    BGwinodwView.backgroundColor =[UIColor grayColor];
    BGwinodwView.alpha = 0.5;
    
    if (temp == nil) {
        UIWindow *tem= [[UIWindow alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        tem.backgroundColor = RGB(0, 0, 0, 1);
        [tem addSubview:BGwinodwView];
        progressHUD = [MBProgressHUD showHUDAddedTo:tem animated:YES];
    }else {
        [temp addSubview:BGwinodwView];
        progressHUD = [MBProgressHUD showHUDAddedTo:temp animated:YES];
    }
    progressHUD.delegate = self;
    progressHUD.removeFromSuperViewOnHide = YES;
    progressHUD.detailsLabelText = [NSString stringWithFormat:@"%@...",NSLocalizedString(@"请稍候", nil)];
    progressHUD.dimBackground =YES;
    [progressHUD hide:YES afterDelay:20];//计时2s
    [progressHUD show:YES];

}
/// 移除加载
- (void)removeDoLoading {
    [BGwinodwView removeFromSuperview];
    [progressHUD removeFromSuperview];
    progressHUD  = nil;
    BGwinodwView = nil;
    
}

+ (int)judgeLocalLanguage {
    int i = 0;
    NSString *nsLang = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    if ([nsLang isEqualToString:@"zh"]) {
        i = 1;
    }else {
        i = 2;
    }
    return i;
}

// 提示弹出框
+ (void)addDisapperAlert: (NSString * _Nullable)str withMessage:(NSString * _Nullable)mess {
    str = [NSString stringWithFormat:@"%@",str];
    if ( ![str isEqualToString:@""] || str.length != 0 ) {
            UIAlertView  *alert2=[[UIAlertView alloc]initWithTitle:str message:mess delegate:nil cancelButtonTitle:NSLocalizedString(@"确定", nil) otherButtonTitles:nil, nil];
            [alert2 show];
    }else{
        [MBProgressHUD yty_showErrorWithTitle:@"" detailsText:mess toView:nil];
    }
    
    
}

+(UIAlertController *)addViewController:(UIViewController *)vc
                           withTitleStr:(NSString *)str
                            withMessage:(NSString *)mess
                             withAction:(UIAlertAction *)action
                           withOKAction:(UIAlertAction *)okAction
                              withStyle:(UIAlertControllerStyle)style {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:str message:mess preferredStyle:style];
    
    UIAlertAction *cancelAction;
    UIAlertAction *ok;
    if (action != nil) {
     cancelAction = action;
     
    }else {
      cancelAction =  [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    }
    
    if (okAction != nil) {
      ok =  okAction;
        
    }else {
      ok =  [UIAlertAction actionWithTitle:NSLocalizedString(@"sure", nil) style:UIAlertActionStyleDefault handler:nil];
    }
    

    [alert addAction:cancelAction];
    [alert addAction:ok];
    [vc presentViewController:alert animated:YES completion:nil];
    return alert;
}


// 添加手势隐藏键盘 -- 自己实现方法
+ (void)addTapGestureRecognizerForTarget:(nullable id)targert toView:(UIView *)view toAction:(nullable SEL)action {
    //手势隐藏键盘
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:targert action:action];
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [view addGestureRecognizer:tapGestureRecognizer];
}
+ (void)removeTapGestureRecognizerForTarget:(nullable id)targert toView:(nullable UIView *)view toAction:(nullable SEL)action {
   // removeGestureRecognizer:(UIGestureRecognizer*)gestureRecognizer
}

+ (UIAlertController *)addAlertControllerForAuthorizationStatus:( NSString *)alertControllerMessages
{
    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:NSLocalizedString(alertControllerMessages, nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    UIAlertAction *sure   = [UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", nil)
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url];
        }
    }];
    [alert1 addAction:cancel];
    [alert1 addAction:sure];
    return alert1;
}


+ (nonnull NSString *)stringFromDate:(nonnull NSDate *)date withDateFormat:(NSString *)format
{
    if (!date) {
        return nil;
    }
    //用于格式化NSDate对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设置格式：zzz表示时区
    NSTimeZone *timzone = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:timzone];
    //@"yyyy-MM-dd HH:mm:ss"
    [dateFormatter setDateFormat:format];
    //NSDate转NSString
    NSString *currentDateString = [dateFormatter stringFromDate:date];
    return currentDateString;
}

//NSString转NSDate
+ (NSDate *)dateFromString:(NSString *)string  withDateFormat:(NSString *)format
{
    //设置转换格式
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    NSTimeZone *timzone =  [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timzone];
//    @"yyyy-MM-dd HH:mm:ss"
    [formatter setDateFormat:format];
    //NSString转NSDate
    NSDate *date=[formatter dateFromString:string];
    if (!date) {
        return nil;
    }
    date = [NSDate dateWithTimeIntervalSince1970:[date timeIntervalSince1970]];
    return date;
}


+ (nonnull NSString *)timeAgeWithChinaTimeTransformCurrentPhoneTimeZooe:(nonnull NSString *)timeString
{
    NSString *dateTime = @"";
    if ([timeString isEqualToString:@""] || timeString == nil || [timeString isEqual:[NSNull null]]) {
        dateTime = @"---";
        
    }else {
    
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timzone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timzone];
    NSDate *date = [formatter dateFromString:timeString];
    date = [NSDate dateWithTimeIntervalSince1970:[date timeIntervalSince1970]];
        dateTime = [date dateTimeAgo];
        return dateTime;
    }
    return dateTime;
}

+ (NSDate *)timestampStringTransformTimeCurrentPhoneTimeZooe:(NSString *)timestampString{

    if (!timestampString) {
        return nil;
    }
    NSInteger timestamp =[timestampString integerValue];
    NSDate *date=[NSDate dateWithTimeIntervalSince1970:timestamp];
    return date;
}


+ (NSString *)timeTransFormStringFormatForLess24h:(NSDate *)time
{
    if (!time) {
        return @"  ---";
    }
    NSString *dateString;
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *temp = [formatter stringFromDate:currentTime];
    temp = [temp stringByAppendingString:@" 00:00:00"];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    currentTime = [formatter dateFromString:temp];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    double deltaSeconds = [time timeIntervalSinceDate:currentTime];
    if (deltaSeconds >=0) {
        
        [formatter setDateFormat:@"HH:mm"];
        dateString = [formatter stringFromDate:time];
    }else{
        [formatter setDateFormat:@"d/MM/yyyy"];
        dateString = [formatter stringFromDate:time];
    }
    return dateString;
}


+ (nonnull NSString *)timestampStringTransformDateString:(long long) timestampTime{
    NSTimeInterval time = timestampTime / 1000 ;
    
    NSDate * detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter*dateFormatter = [[NSDateFormatter alloc]init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateSMS = [dateFormatter stringFromDate:detaildate];
    
    NSDate *now = [NSDate date];
    NSString *dateNow = [dateFormatter stringFromDate:now];
    if ([dateSMS isEqualToString:dateNow]) {
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    else {
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    //    NSLog(@"显示的时间 ： %@",[dateFormatter stringFromDate:detaildate]);
    return  [dateFormatter stringFromDate:detaildate];
}


+ (NSString*)getPreferredLanguage
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    return preferredLang;
}

@end
