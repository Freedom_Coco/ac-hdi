//
//  AppDelegate.m
//  MonitoringSystem
//
//
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖保佑             永无BUG
#import "AppDelegate.h"
#import <SMS_SDK/SMSSDK.h>
#import <ALBBPush/CloudPushSDK.h>
#import <ALBBSDK/ALBBSDK.h>
#import <SDWebImage/SDImageCache.h>
#import "ACDeviceController.h"
#import "ACUserLoginManage.h"
#import "ACMainViewController.h"
#import "ACNewGuidController.h"
#import "ACUserLoginManage.h"
@interface AppDelegate ()<UIAlertViewDelegate>
@end
@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // 启动页的时候隐藏导航条
    [application setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    
    // 统一按钮的clear的样式 自定义清楚按钮
    [self setUpTextFieldClearnButton];
    
    // 更新cookit操作
    [self updateSession];
    
    //初始化SMSDK   ------------ 用于短信验证
    [SMSSDK registerApp:MOBSMSAPPKEY withSecret:MOBSMSAPPSECRET];
    
    //初始化 云推送  ---------------------   推送  ****  注意推送有一张纹理识别身份，跟ID绑定了一起，因为要对应的ID生成对应的SDK *****
    [self setUpOpenIMPushService:application didFinishLaunchingWithOptions:launchOptions];
    
    // 清楚缓存
    [self clearTmpPics];
    
    // 选择根控制器
    [self chooseRootViewControlller];
    return YES;
}


- (void) chooseRootViewControlller{
    // 已经登录过
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    ACUserLoginManage * loginManage = [ACUserLoginManage shareInstance];
    UIStoryboard * storyboard;
    if (loginManage.userCode) {
        storyboard =  [UIStoryboard storyboardWithName:@"Main" bundle:nil];
         self.window.rootViewController = [storyboard instantiateInitialViewController];
    }else{
        ACNewGuidController * newGuidController = [[ACNewGuidController alloc] init];
        self.window.rootViewController = newGuidController;
    }
    [self.window makeKeyWindow];
    [self.window makeKeyAndVisible];

}

-(void)applicationDidBecomeActive:(UIApplication *)application{
    [UD setObject:@(0) forKey:@"applicationIconBadgeNumber"];
    application.applicationIconBadgeNumber = 0;
}

#pragma mark 清除缓存
- (void)clearTmpPics{
    
    [[SDImageCache sharedImageCache] clearDisk];
    [[SDImageCache sharedImageCache] clearMemory];//可有可无
}


#pragma mark 内存警告
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
    
    [[SDImageCache sharedImageCache] clearDisk];
    [[SDImageCache sharedImageCache] clearMemory];//可有可无
}

#pragma mark 设置textField上的清除按钮的样式
-(void) setUpTextFieldClearnButton {
    UIButton *defaultClearButton = [UIButton appearanceWhenContainedIn:[UITextField class], nil];
    defaultClearButton = [UIButton appearanceWhenContainedIn:[UITextField class], nil];
    [defaultClearButton setBackgroundImage:[UIImage imageNamed:@"TextfiledClose"] forState:UIControlStateNormal];
}

/**
 *  http://www.jianshu.com/p/65094611980c
 *
 *  cookie的操作.更新cookie。在程序每次启动的时候调用一下。用来确保每次的cookie是最新的。
 */
- (void) updateSession{
    NSDictionary * cookitDic =  [UD dictionaryForKey:acLocalCookieName];
    NSDictionary * cookitProperties = [cookitDic valueForKey:acCookitDict];
    if (cookitProperties != nil) {
        NSHTTPCookie * cookie = [NSHTTPCookie cookieWithProperties:cookitProperties];
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    }
}


#pragma mark  ********************   云推送服务 sdk的方法 有此处开始 *************************************
- (void) setUpOpenIMPushService:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    // 注册apns
    [self registerAPNS:application :launchOptions];
    // 初始化sdk
    [self init_tae];
    // 监听云通道
    [self listenerOnChannelOpened];
    // 接受推送消息
    [self registerMsgReceive];
}


#pragma mark 初始化应用
- (void)init_tae{
    [[ALBBSDK sharedInstance] setDebugLogOpen:NO];//开发阶段打开日志开关，方便排查错误信息
    [[ALBBSDK sharedInstance] asyncInit:^{
        NSLog(@"======================> 初始化成功");
        NSLog(@"======================> DeviceID：%@", [CloudPushSDK getDeviceId]);
          NSLog(@"======================> getVersion：%@", [CloudPushSDK getVersion]);
    }failure:^(NSError *error) {
        NSLog(@"======================> 初始化失败:%@",error);
    }];
}


- (void) listenerOnChannelOpened {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onChannelOpened:) name:@"CCPDidChannelConnectedSuccess" object:nil]; // 注册
}
#pragma mark 推送下来的消息抵达的处理示例
- (void)onChannelOpened:(NSNotification *)notification {
    NSLog(@"消息通道建立成功");
}




#pragma mark 注册苹果的推送
-(void) registerAPNS :(UIApplication *)application :(NSDictionary *)launchOptions{
     /**
        注册apns
     */
    /// 需要区分iOS SDK版本和iOS版本。
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [application registerForRemoteNotifications];
    }
    else{
        // iOS < 8 Notifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
    [CloudPushSDK handleLaunching:launchOptions]; // 作为 apns 消息统计
}


#pragma mark 注册接收CloudChannel 推送下来的消息
- (void) registerMsgReceive {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageReceived:) name:@"CCPDidReceiveMessageNotification" object:nil]; // 注册
}


// 推送下来的消息抵达的处理示例）
- (void)onMessageReceived:(NSNotification *)notification {
     NSData *receviceData = [notification object];
    NSLog(@"-->> 推送过来的消息体:message body:%@",receviceData);
    NSError * error = nil;
    NSDictionary *receviceDic = [NSJSONSerialization JSONObjectWithData:receviceData options:NSJSONReadingMutableContainers error:&error];//转换数据格式
    if (error) {
        // 解析失败
        NSLog(@"解析失败");
        return;
    }
    NSLog(@"-->> 解析成功:message :%@",receviceDic);
    if (![receviceDic[@"account"] isEqualToString:[ACUserLoginManage shareInstance].userCode]) {// 如果推送的账号，跟当前登录账号相等，那么就推送
        return;
    }
    // 下标值处理
    int numberBadge = [[UD objectForKey:@"applicationIconBadgeNumber"] intValue];
    [UIApplication sharedApplication].applicationIconBadgeNumber = numberBadge + 1;
    [UD setObject:@(numberBadge + 1) forKey:@"applicationIconBadgeNumber"];
    // 推送内容
    NSString *message = [NSString stringWithFormat:@"%@",receviceDic[@"msg"] ];
    int result = [receviceDic[@"result"] intValue];
    // 报警提示
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (message != nil) {
                NSLog(@"-->> push %@", message);
            }
            [self showAlertWithPushDeviceContent:result message:message];
        });
    } else {
        [self showAlertWithPushDeviceContent:result message:message];
    }
}

/**
 *  根据推送的内容显示不一样的框  0-管理员转移   1-非作业状态推送   2-马达故障推送
 */
- (void) showAlertWithPushDeviceContent:(NSInteger) result message:(NSString *)message{
    
    UIAlertView *alertView = [[UIAlertView alloc] init];
    alertView.title = NSLocalizedString(@"接收到一条消息",nil);
    alertView.delegate = self;
    if (result == 0) {
        [alertView addButtonWithTitle:NSLocalizedString(@"确定",nil)];
        alertView.message = [NSString stringWithFormat:@"您已成为设备编号为:%@的管理者",message];
    }else if(result == 1 ){
        [alertView addButtonWithTitle:NSLocalizedString(@"取消",nil)];
        [alertView addButtonWithTitle:NSLocalizedString(@"确定",nil)];
        alertView.message = [NSString stringWithFormat:@"设备编号为:%@的设备进入非作业状态",message];
    }else if(result == 2){
        [alertView addButtonWithTitle:NSLocalizedString(@"取消",nil)];
        [alertView addButtonWithTitle:NSLocalizedString(@"确定",nil)];
        alertView.message = [NSString stringWithFormat:@"设备编号为:%@的设备进入马达故障状态",message];
    }else if(result == 3){
        [alertView addButtonWithTitle:NSLocalizedString(@"取消",nil)];
        [alertView addButtonWithTitle:NSLocalizedString(@"确定",nil)];
        alertView.message = [NSString stringWithFormat:@"设备编号为:%@的设备进入低油位状态",message];
    }
    alertView.tag = result;
    [alertView show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 0){
        // 权限转移下来
            // 点击确定，刷新设备列表.storyboard
            UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            [[[UIApplication sharedApplication] delegate] window].rootViewController =  [storyBoard instantiateInitialViewController];
    }else  if(alertView.tag == 1 || alertView.tag == 2){
        // 故障、非作业下来
        if (buttonIndex == 1) {
            // 点击确定，刷新设备列表.storyboard
            UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            [[[UIApplication sharedApplication] delegate] window].rootViewController =  [storyBoard instantiateInitialViewController];
        }
    }
}

#pragma mark  ********************      云推送服务  系统方法从此处开始   *************************************
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}


// 苹果推送服务回调，返回deviceToken.注册deviceToken给云推送
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

    NSString *token =   [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"withString:@""]
                          stringByReplacingOccurrencesOfString:@">" withString:@""]
                         stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"deviceToken:%@",token);
    [CloudPushSDK registerDevice:deviceToken];// 第三方通知APNS下达推送通知
    
}



//通知统计回调   消息下来的时候，回调此处两个方法
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo{
    [CloudPushSDK handleReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [CloudPushSDK handleReceiveRemoteNotification:userInfo];
   completionHandler(UIBackgroundFetchResultNewData);
    NSLog(@"userInfo --------------------- %@:",userInfo);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error.description);
}

- (void)applicationWillResignActive:(UIApplication *)application {}
- (void)applicationDidEnterBackground:(UIApplication *)application {}
- (void)applicationWillEnterForeground:(UIApplication *)application {}
- (void)applicationWillTerminate:(UIApplication *)application {}
@end
