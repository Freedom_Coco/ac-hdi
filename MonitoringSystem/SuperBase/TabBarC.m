//
//  TabBarC.m
//  DataStatistics
//
//  Created by Kang on 15/12/25.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "TabBarC.h"


@interface TabBarC ()
@property (copy, nonatomic) NSString *NavigationItemTitle;
@property (weak, nonatomic) UIButton *selectedBtn;
@end

@implementation TabBarC


+ (void)initialize {
    NSMutableDictionary *dir1 = [NSMutableDictionary dictionary];
    dir1[NSForegroundColorAttributeName] = RGB(42, 192, 228, 1);
    
    [[UITabBar appearance] setSelectedImageTintColor:RGB(42, 192, 228, 1)];
    [[UITabBarItem appearance].image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [[UITabBarItem appearance].selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [[UITabBar appearance] setBarTintColor:RGB(25, 25, 25, 1)];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSMutableDictionary dictionaryWithObject:RGB(171, 171, 171, 1) forKey:NSForegroundColorAttributeName] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:dir1 forState:UIControlStateSelected];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
  
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}



@end
