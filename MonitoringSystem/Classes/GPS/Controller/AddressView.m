//
//  AddressView.m
//  DataStatistics
//
//  Created by oilklenze on 16/3/25.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "AddressView.h"
#import "Masonry.h"
#import <pop/POP.h>
#import <CoreLocation/CoreLocation.h>
@import AddressBookUI;
@interface AddressView ()
@end

@implementation AddressView


- (instancetype)init
{
    self = [super init];
    if (self !=nil) {
        
        _activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _activity.center =self.center;
        _activity.width  = 20;
        _activity.height = 20;
        [_activity startAnimating];
        [self setImage:[UIImage imageNamed:@"地址背景"]];
         self.userInteractionEnabled = YES;
        
        if (!_address) {
            _address = [[UILabel alloc] init];
            [_address setTextColor:RGB(200, 200, 200, 0.95)];
            [self addSubview:_address];
            [_address setNumberOfLines:0];
            [_address mas_makeConstraints:^(MASConstraintMaker* make) {
                make.left.equalTo(self).with.offset(60);
                make.bottom.equalTo(self);
                make.right.equalTo(self).with.offset(-8);
                make.top.equalTo(self);
            }];
            [_address setFont:[UIFont systemFontOfSize:12]];
            [_address addSubview:_activity];
            [_activity mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(self);
                make.size.mas_equalTo(CGSizeMake(20, 20));
            }];
        }
        if (!_navigationBtm) {
            _navigationBtm = [[UIButton alloc] init];
            [self addSubview:_navigationBtm];
            [_navigationBtm setBackgroundImage:[UIImage imageNamed:@"导航按钮"] forState:UIControlStateNormal];
            [_navigationBtm mas_makeConstraints:^(MASConstraintMaker* make) {
                make.left.equalTo(self);
                make.top.equalTo(self);
                make.bottom.equalTo(self);
                make.width.mas_equalTo(@52);
            }];
        }

    }
    return self;
}

+ (void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude addrees:(place)address{
    //反地理编码22.22177,113.48168333333334
    CLGeocoder* geocoder = [[CLGeocoder alloc] init];
    CLLocation* location = [[CLLocation alloc] initWithLatitude:22.22177 longitude: 113.48168333333334];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray* placemarks, NSError* error) {
        
        if (error||placemarks.count==0) {
            NSLog(@"你输入的地址没找到，可能在月球上 %@",error);
            return ;
      }
        CLPlacemark* placemark = [placemarks firstObject];
        address(placemark.addressDictionary);
    }];
    
}

- (void)addAppearAnimationByBeginFrame:(CGRect)begin finishFrame:(CGRect)finsish
{
    POPBasicAnimation* anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
    anim.fromValue = [NSValue valueWithCGRect:begin];
    anim.toValue = [NSValue valueWithCGRect:finsish];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.duration = 0.5;
    [self pop_addAnimation:anim forKey:nil];
 
}

- (void)labelSetAddress:(NSString *)newAddress{
    
    if ([newAddress isEqualToString:@""] || !newAddress) {
        [self.activity startAnimating];
    }else{
        [self.activity stopAnimating];
    }

    self.address.text =newAddress;
}

@end
