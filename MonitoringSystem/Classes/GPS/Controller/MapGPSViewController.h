//
//  MapGPSViewController.h
//  DataStatistics
//
//  Created by oilklenze on 15/11/3.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import "AddressView.h"

@interface MapGPSViewController : UIViewController

@property (strong, nonatomic) MKMapView *mapview;
@property (retain, nonatomic) NSString  *device_IMEI;
@property (strong, nonatomic) AddressView *addresView;

/**
 *  从设置界面中进去
 */
@property(nonatomic,assign)BOOL isEnterSetting;
@end
