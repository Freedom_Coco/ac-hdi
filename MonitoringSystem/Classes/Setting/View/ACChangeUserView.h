//
//  ACChangeUserView.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/16.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeUserViewDelegate <NSObject>
- (void) judgeToChangePwdOrLogOut:(BOOL) isLogout;

@end

@interface ACChangeUserView : UIView


@property(nonatomic,assign) id<ChangeUserViewDelegate> delegate;


/**
 *  在创建视图大小的时候，根据子视图的个数来决定其大小的位置
 *
 */
- (instancetype)initWithFrame:(CGRect)frame withsubView:(NSInteger)objSub;

/**
 *  根据传入的按钮、文字来创建子控件。只开放给按钮
 *
 */
- (void) initButtonSubViewWithImage:(UIImage *)img buttonTitle:(NSString *)title;
@end
