//
//  ACRecommedController.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/7/10.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACRecommedController : UIViewController

/**
 *  设备编号
 */
@property (nonatomic,strong) NSString * imei;




@property(nonatomic,assign) BOOL   isEmployer; // 是否是管理者

@property(nonatomic,strong) NSString *  employer;// 如果是管理者，那么号码是多少

@property(nonatomic,strong) NSString * zone;// 管理者的区号


@property(nonatomic,copy) GetDeviceConfig  pushDeviceName;
- (void)returnText:(GetDeviceConfig)block;

@end
