//
//  ACRecommedHeadView.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/11.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import "ACRecommedHeadView.h"

@implementation ACRecommedHeadView

- (instancetype)initWithTitle:(NSString *) title withImage:(UIImage *)imgView{
    if (self = [super init]) {
        self.bounds = CGRectMake(0, 0, SCREEN_WIDTH, 60);
        self.backgroundColor = [UIColor colorWithRed:31/ 255.0 green:36 / 255.0 blue:46 / 255.0 alpha:1];
        // 图片
        CGFloat imageViewW = 22;
        CGFloat imageViewX = 10;
        UIImageView * imageView = [[UIImageView alloc] initWithImage:imgView];
        imageView.frame = CGRectMake(imageViewX, self.bounds.size.height * 0.5 - imageViewW * 0.5, imageViewW, imageViewW);
        [self addSubview:imageView];
        
        //文字
        UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + 10, CGRectGetMidY(imageView.frame) - 15 ,  100, 30)];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.font = [UIFont systemFontOfSize:18];
        titleLabel.text = title;
  
        [self addSubview:titleLabel];
        // 下划线
        UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(imageViewX, CGRectGetMaxY(imageView.frame) + 5, self.frame.size.width - imageViewX * 2, 1)];
        lineView.backgroundColor = [UIColor whiteColor];
        lineView.alpha = 0.3;
        [self addSubview:lineView];
    }
    return self;
}


@end
