//
//  ACRecommedViewCell.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/11.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACRecommedViewCell : UITableViewCell
/**
 *  配置数据
 */
@property(nonatomic,strong) NSDictionary * configDic;

@end
