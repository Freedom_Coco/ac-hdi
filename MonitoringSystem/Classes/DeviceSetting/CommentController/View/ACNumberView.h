//
//  ACNumberView.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/30.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACPortBtnBase.h"
@interface ACNumberView : ACPortBtnBase

/**
 *  设置外围圈上的按钮，数字、旋转度
 *
 *  @param obj   当前谁的子控件
 *  @param text  内容
 *  @param angle 角度
 */
- (void) setLableWithCountNumber:(id) obj andText:(NSString *)text andAngle:(CGFloat) angle;

@end
