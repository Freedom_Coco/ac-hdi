//
//  ACNumberView.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/30.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
#import "ACNumberView.h"
@interface ACNumberView ()

@property(nonatomic,weak) UILabel * lblCount;
@end


@implementation ACNumberView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUpChildView];
    }
    return self;
}

-(void) setUpChildView{
    
    UILabel * lblCount = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width * 0.05 , 3, self.frame.size.width * 0.9, self.frame.size.width * 0.9)];
    lblCount.textAlignment = NSTextAlignmentCenter;
    lblCount.font = [UIFont boldSystemFontOfSize:16];
    [self addSubview:lblCount];
    lblCount.textColor = [UIColor whiteColor];
    _lblCount = lblCount;
    
}

- (void)setLableWithCountNumber:(id)obj andText:(NSString *)text andAngle:(CGFloat)angle{
    _lblCount.text = text;
}
@end
