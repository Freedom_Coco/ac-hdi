//
//  ACDeviceSettingViewCell.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/28.
//  Copyright © 2016年 YTYangK. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ACCircleView.h"


@protocol DeviceSettingCellProtocol <NSObject>

- (void) showRecommedCallBack;

@end
@interface ACDeviceSettingViewCell : UICollectionViewCell

/**
 *  当前页数
 */
@property (nonatomic,assign) NSInteger currIndex;

@property (nonatomic,weak) UICollectionView * collectionView;
/**
 *  端口总数
 */
@property (nonatomic,assign) NSInteger portLayer;

/**
 *   是不是管理员
 */
@property(nonatomic,assign) BOOL isEmploy;


@property(nonatomic,strong)NSString * zone;


@property(nonatomic,strong) NSString * employ;



- (void) setUpWith:(UICollectionView *)collection andIndex:(NSInteger) currIndex andPortLayer:(NSInteger ) portLayer;
/**
 *  AC泵芯的配置数据
 */
@property (nonatomic,strong) NSArray * configData;
@property (nonatomic,strong) ACCircleView * circleView;

@property(nonatomic,assign) id< DeviceSettingCellProtocol> delegate;

- (void) openSelectPortMaskLayerForGuid4;
@end
