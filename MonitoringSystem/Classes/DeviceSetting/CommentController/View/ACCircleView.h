//
//  ACCircleView.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/29.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ACCircleViewForCallBackPort <NSObject>

/**
 *  回调到Cell中。并将数据保存到本地。
 *
 *  @param index 位置
 *  @param layer 层级数
 *  @param tag   端口颜色标记
    oldColorTag 当前选中的按钮原来的颜色标记
 */
- (void) portCallBackIndex:(NSInteger )index andLayer:(NSInteger) layer andTag:(NSInteger) tag andOldColorTag:(NSInteger)oldColorTag;

@end


@interface ACCircleView : UIView

@property (assign) id<ACCircleViewForCallBackPort> circleViewDelegate;

/**
 *  当前的层数
 */
@property (nonatomic,assign) NSInteger currLayer;

/**
 *   是不是管理员
 */
@property(nonatomic,assign) BOOL isEmploy;


@property(nonatomic,strong)NSString * zone;


@property(nonatomic,strong) NSString * employ;
/**
 *  加载配置表里面的信息
 *
 *  @param pos      位置
 *  @param colorTag 颜色
 */
- (void) configCurrentInformationWithPos:(NSInteger) pos andColorTag:(NSInteger)colorTag andCurrLayer:(NSInteger) currLayer andIndexLayer:(NSInteger)indexLayer;

@property(nonatomic,weak) UICollectionView * cirCollectionView;

/**
 *  开启引导
 */
- (void) openSelectPortMaskLayerForGuid4;

/**
 *
 *  计算冲程输出
 */
- (void) calcalatePortOutPut:(NSInteger) colorTag;
@end
