//
//  ACPortBtnBase.h
//  MonitoringSystem
//
//  Created by oilklenze on 16/6/15.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACPortBtnBase : UIButton

/**
 *   颜色
 */
@property(nonatomic,assign)PORTCOLOR colorTag;
@end
