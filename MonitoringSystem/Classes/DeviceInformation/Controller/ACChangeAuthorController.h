//
//  ACChangeAuthorController.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/26.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACChangeAuthorController : UIViewController

/**
 *  设备IMEI
 */
@property(nonatomic,strong) NSString * imei;

/**
 *  设备详情进入
 */
@property(nonatomic,assign) BOOL isDeviceInfoEnter;


@property(nonatomic,assign) int bindings;
@end
