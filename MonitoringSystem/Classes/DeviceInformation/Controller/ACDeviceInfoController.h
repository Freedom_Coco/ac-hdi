//
//  ACDeviceInfoController.h
//  MonitoringSystem
//
//  Created by oilklenze on 16/6/21.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACDeviceInfoController : UIViewController

/**
 *  设备编号
 */
@property(nonatomic,strong) NSString * i_imei;

@end
