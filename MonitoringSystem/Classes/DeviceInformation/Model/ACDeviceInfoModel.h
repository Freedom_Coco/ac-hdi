//
//  ACDeviceInfoDevice.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/22.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ACDeviceInfoModel : NSObject

/**
 *  剩余油量
 */
@property(nonatomic,assign)CGFloat end_oil;

/**
 *  每天工作时间，用于计算剩余天数，剩余油量
 */
@property(nonatomic,assign) int setTime;

/**
 *  剩余天数
 */
@property(nonatomic,assign)CGFloat end_day;

/**
 *  设备当前所处的状态进行到什么时间了。
 */
@property(nonatomic,assign)int runTime;
/**
 *  每天打油量
 */
@property(nonatomic,assign) CGFloat averageOil;

/**
 *  设备昵称
 */
@property(nonatomic,strong) NSString * nickname;


/**
 *  端口数
 */
@property(nonatomic,assign) NSInteger number;

/**
 *  档位。注意档位1档表示实际泵芯的0档（档位数都降1计算）
 */
@property(nonatomic,assign) NSInteger shift;


/**
 *  状态
 */
@property(nonatomic,assign)    int i_state;


/**
 *  设备类型
 */
@property(nonatomic,strong) NSString * type;
/**
 *  设备拥有者的userID
 */
@property(nonatomic,strong) NSString * employer;

@property(nonatomic,strong) NSString * zone;

/**
 *  当前登陆对象是否是该设备的管理者
 */
@property(nonatomic,assign) BOOL isEmployer;



-(instancetype) initWithDic:(NSDictionary *)dic;

+ (instancetype) deviceInfoWithDic:(NSDictionary *)dic;

@end
