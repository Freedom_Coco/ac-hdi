//
//  ACSettingWorkTimeView.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/6/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ACSettingWorkTimeView;
@protocol SettingWorkingTimeDelegate <NSObject>

- (void) settingWorkingTime:(ACSettingWorkTimeView *) view ButtonCallBack:(NSInteger)time;

@end

@interface ACSettingWorkTimeView : UIView

@property(nonatomic,assign)id<SettingWorkingTimeDelegate> delegate;
@end
