//
//  ACSettingWorkTimeView.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/6/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import "ACSettingWorkTimeView.h"


@interface ACSettingWorkTimeView ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    NSInteger chooseIndex;
}
@property(nonatomic,weak) UIView * maskLayer;
@property(nonatomic,strong) NSMutableArray * contentArr;// 内容
@end

@implementation ACSettingWorkTimeView

- (NSMutableArray *)contentArr{
    if (!_contentArr) {
        _contentArr = [NSMutableArray array];
    }
    return _contentArr;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
  
        [self setUpChildView];
        
    }
    return self;
}


- (void) setUpChildView{
    for (int index = 0; index <= 24; index++) {
        [self.contentArr addObject:[NSString stringWithFormat:@"%d小时",index]];
    }
    
    CGFloat viewW = self.frame.size.width;
    CGFloat viewH = self.frame.size.height;
    
    UIView * maskLayer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    maskLayer.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    [[UIApplication sharedApplication].keyWindow addSubview:maskLayer];
    self.maskLayer = maskLayer;
    
    // 背景
    UIImageView * BG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Setshijian"]];
    BG.frame = CGRectMake(0, 0, viewW, viewH);
    [self addSubview:BG];
    
    CGFloat workTimeH = 40;
    // 标题
    UILabel * setWorkTimeLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, viewW, workTimeH)];
    setWorkTimeLable.text = @"选择每天工作时间";
    setWorkTimeLable.font = [UIFont systemFontOfSize:16];
    setWorkTimeLable.textAlignment = NSTextAlignmentCenter;
    [self addSubview:setWorkTimeLable];
    
    // 日期选择器
    UIPickerView * pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, workTimeH, viewW, viewH - workTimeH * 2)];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [self addSubview:pickerView];
    
    //取消按钮
    UIButton * cancelBtn  = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(pickerView.frame) - 6 , viewW * 0.5,workTimeH + 20)];
    [cancelBtn setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [self addSubview:cancelBtn];
    cancelBtn.tag = 0;
    [cancelBtn addTarget:self action:@selector(ButtonCallBack:) forControlEvents:UIControlEventTouchUpInside];
    
    // 确定按钮
    UIButton * sureBtn  = [[UIButton alloc] initWithFrame:CGRectMake(viewW * 0.5, CGRectGetMaxY(pickerView.frame)- 6, viewW * 0.5,workTimeH + 20)];
    [sureBtn setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [self addSubview:sureBtn];
    sureBtn.tag = 1;
    [sureBtn addTarget:self action:@selector(ButtonCallBack:) forControlEvents:UIControlEventTouchUpInside];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 25;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.contentArr[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    chooseIndex = row;
}

- (void) ButtonCallBack:(UIButton *)sender{
    [self.maskLayer removeFromSuperview];
    [self removeFromSuperview];
    if (sender.tag == 0) {
        return;
    }
    // 跳转到原来的事件发起者
    if ([self.delegate respondsToSelector:@selector(settingWorkingTime:ButtonCallBack:)]) {
        [self.delegate settingWorkingTime:self ButtonCallBack:chooseIndex];
    }
}
@end
