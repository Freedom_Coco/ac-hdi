//
//  ACMainNavigationController.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/25.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACMainNavigationController.h"

@interface ACMainNavigationController ()

@end

@implementation ACMainNavigationController

+ (void)initialize{
#pragma mark 设置样式 状态栏
    UINavigationBar *navBar = [UINavigationBar appearanceWhenContainedIn:[ACMainNavigationController class], nil];
    //针对NavigationC  修改
#pragma mark 设置item
    NSDictionary * titleAttr = @{NSForegroundColorAttributeName:[UIColor whiteColor],
                                 NSFontAttributeName:[UIFont systemFontOfSize:18]};
    [navBar setTitleTextAttributes:titleAttr];
    // tintColor 是用于导航条所有item的颜色
      navBar.tintColor = [UIColor whiteColor];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationBarHidden = YES;
}

/*
- (void)viewDidLoad {
    [super viewDidLoad];
  
    CGRect frame = self.navigationBar.frame;
    _alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height+20)];
    _alphaView.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:_alphaView belowSubview:self.navigationBar];
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"bigShadow.png"] forBarMetrics:UIBarMetricsCompact];
    self.navigationBar.layer.masksToBounds = YES;

}


-(void)setAlph{
    if (_changing == NO) {
        _changing = YES;
        if (_alphaView.alpha == 0.0 ) {
          
                _alphaView.alpha = 1.0;
        
                _changing = NO;
         
        }else{
           
                _alphaView.alpha = 0.0;
       
                _changing = NO;

        }
    }
    
    
}
*/


#pragma mark 当导航控制器的子控制器被pop移除的时候回被调用
- (UIViewController *)popViewControllerAnimated:(BOOL)animated{
    return  [super popViewControllerAnimated:animated];
}


#pragma mark 当导航控制器的子控制器被push的时候回被调用
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    // push控制器的时候，隐藏下面的状态栏
    viewController.hidesBottomBarWhenPushed = YES;
    [super pushViewController:viewController animated:animated];
}

@end
