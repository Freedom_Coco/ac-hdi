//
//  ACChangeDeviceNameController.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/4.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACChangeDeviceNameController : UIViewController

@property(nonatomic,strong) NSString * oldName;

@property(nonatomic,copy) PushDeviceName  pushDeviceName;
- (void)returnText:(PushDeviceName)block;
@end
