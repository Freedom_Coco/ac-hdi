//
//  ACCustomTextView.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACCustomSearchBar.h"

@implementation ACCustomSearchBar

-(instancetype)initWithFrame:(CGRect)frame{
    
    if(self = [super initWithFrame:frame]){
        self.font = [UIFont systemFontOfSize:14];
        UIImageView * icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchbar_textfield_search_icon"]];
        icon.frame = CGRectMake(5, self.bounds.size.height * 0.5 - 10, 20, 20);
        [self addSubview:icon];
        self.textContainerInset = UIEdgeInsetsMake(self.bounds.size.height * 0.5 - 10, 20, 0, 0);
        self.layer.cornerRadius = 10;
        self.font = [UIFont systemFontOfSize:16];
    }
    return self;
    
}

@end
