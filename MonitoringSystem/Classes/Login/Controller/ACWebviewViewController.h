//
//  ACWebviewViewController.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACWebviewViewController : UIViewController

/**
 *  请求的url
 */
@property (nonatomic,strong) NSString * url;
@end
