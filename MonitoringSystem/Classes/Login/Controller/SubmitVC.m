//
//  SubmitVC.m
//  DataStatistics
//
//  Created by Kang on 16/3/10.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "SubmitVC.h"
#import "isPhoneNumber.h"
#import "UtilToolsClass.h"
#import "NetRequestClass.h"
@interface SubmitVC () <UITextFieldDelegate, UIAlertViewDelegate> {
    BOOL isSVCClick;
}
@end

@implementation SubmitVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateUI];
    self.title = NSLocalizedString(@"设定密码", nil);
    self.pasText.text = NSLocalizedString(@"SETPASSWORD-PASLABLE", nil);
    //自动折行设置
    self.pasText.lineBreakMode = NSLineBreakByWordWrapping;
    self.pasText.numberOfLines = 0;

    UIBarButtonItem* backIetm = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back@3x"] style:UIBarButtonItemStylePlain target:self action:@selector(backItemClick)];
    self.navigationItem.leftBarButtonItem = backIetm;
    self.navigationItem.hidesBackButton = YES;
}

-(void)backItemClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _pasField.delegate = self;
    _againPasField.delegate = self;
    [_pasField addTarget:self action:@selector(SVCtextLengthMethod:) forControlEvents:UIControlEventEditingChanged];
    [_againPasField addTarget:self action:@selector(SVCtextLengthMethod:) forControlEvents:UIControlEventEditingChanged];
}


- (void)updateUI{
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide)];
    tapGestureRecognizer.cancelsTouchesInView = YES;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    _pasField.placeholder = NSLocalizedString(@"请输入6-12位数的密码", nil);
    self.pasField.secureTextEntry = YES;
    _againPasField.placeholder = NSLocalizedString(@"再输入一次密码", nil);
    self.againPasField.secureTextEntry = YES;
    self.okBtn.enabled = NO;
    [self.okBtn setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
    _userPhone.text = _SVC_phoneNumber;
}

- (void)keyboardHide{
    [self.view endEditing:YES];
}

// 限制长度
- (void)SVCtextLengthMethod:(UITextField*)textField{
    [self setSendViewBtnShow];
    if (textField == self.pasField) {
        if (textField.text.length > 12) {
            textField.text = [textField.text substringToIndex:12];
        }
    }
    else if (textField == self.againPasField) {
        if (textField.text.length > 12) {
            textField.text = [textField.text substringToIndex:12];
        }
    }
}
/// 控制 发送按钮是否显示
- (void)setSendViewBtnShow{
    if (self.pasField.text.length >= 6  && self.againPasField.text.length >= 6) {
        self.okBtn.enabled = YES;
    }else {
        self.okBtn.enabled = NO;
    }
}

- (void)textFieldDidBeginEditing:(UITextField*)textField{
    if (!isSVCClick) {
        isSVCClick = YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if (textField == _pasField) {
        [_againPasField becomeFirstResponder];
    }
   
    if (textField == _againPasField) {
        // 实行注册
        [self okMethod:nil];
    }
    return YES;
}

// 监控NickName中文输入方法(限制长度)
- (void)SVCtextfiledEditChanged:(NSNotification*)obj
{

    UITextField* textfield = (UITextField*)obj.object;
    NSString* toBeString = textfield.text;
    // 键盘输入模式
    NSString* lang = [[UITextInputMode currentInputMode] primaryLanguage];
    // 简体中文输入，包括简体拼音，健体五笔，简体手写
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange* selectedRange = [textfield markedTextRange];
        //获取高亮部分
        UITextPosition* position = [textfield positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > 15) {
                // textfield.text = [toBeString substringFromIndex:10]; //如果超过十个重新清空内容，获取后面输入的内容。
                textfield.text = [toBeString substringToIndex:15];
            }
            else {
                // 有高亮选择的字符串，则暂不对文字进行统计和限制
            }
        }
        else {
            // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
            if (toBeString.length > 12)
                textfield.text = [toBeString substringToIndex:12];
        }
    }
    else if ([lang isEqualToString:@"emoji"]) {
        //[UtilToolsClass addViewController:self withTitleStr:NSLocalizedString(@"", nil) withMessage:NSLocalizedString(@"SVCP6", nil)];
         textfield.text = [textfield.text removeEmoji];
    }
    else if ([lang isEqualToString:@"en-US"]) {
        if (toBeString.length > 12)
            textfield.text = [toBeString substringToIndex:12];
    }
    [self setSendViewBtnShow];
}


- (IBAction)okMethod:(UIButton*)sender{
 
    [self.view endEditing:YES];
    NSCharacterSet* whithNewChars = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString* iphonePas = [_pasField.text stringByTrimmingCharactersInSet:whithNewChars];
  
    NSString* againPas = [_againPasField.text stringByTrimmingCharactersInSet:whithNewChars];

    if (iphonePas.length < 6 || againPas.length < 6) {
        [UtilToolsClass addDisapperAlert: @"" withMessage:NSLocalizedString(@"Please enter 6-12 characters", nil)];
        return;
    }
    if (![isPhoneNumber checkPasswordAndNum:iphonePas] && ![isPhoneNumber checkPasswordAndNum:againPas]) { //
        [UtilToolsClass addDisapperAlert: @"" withMessage:NSLocalizedString(@"SETPASSWORD-ILLEGAL", nil)];
    }
    else {
        if ([iphonePas isEqualToString:againPas]) {
            NSDictionary* param = @{ @"loginName" : _SVC_phoneNumber,
                @"zone" : _SVC_areaNam,
                @"nickName" :@"",
                @"verifyCode" : _SVC_SMSCode,
                @"password" : iphonePas };
            NSLog(@"----->%@", param);
            __weak typeof(self) weakSelf = self;
            [[UtilToolsClass getUtilTools] addDoLoading];
            [NetRequestClass requestWithUrlForSetPwd:self andUrl:[REQUESTHEADER stringByAppendingString:@"userInfo/userRegister.asp"] andParam:param success:^(NSInteger result) {
                [[UtilToolsClass getUtilTools] removeDoLoading];
                if (result) {
                    [UtilToolsClass addDisapperAlert:@"" withMessage:NSLocalizedString(@"成功",nil)];
                     [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                }
            } failure:^(NSString *failure) {
                [[UtilToolsClass getUtilTools] removeDoLoading];
                [UtilToolsClass addDisapperAlert: @"" withMessage:failure];
            }];

        }else {
            [UtilToolsClass addDisapperAlert: @"" withMessage:NSLocalizedString(@"两次输入的密码不一致", nil)];
        }
    }
}
@end
