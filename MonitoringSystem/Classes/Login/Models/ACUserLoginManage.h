//
//  ACUserLoginManage.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/26.
//  Copyright © 2016年 YTYangK. All rights reserved.
//


/**
 *   此工具类用于 用户登录信息的保存。实例化为一个单例对象。
 */
#import <Foundation/Foundation.h>

@interface ACUserLoginManage : NSObject


SINGLETON_H(shareInstance)
/**
 *  公司名
 */
@property (nonatomic,strong) NSString * cNo;

/**
 *  账号
 */
@property (copy, nonatomic) NSString *userCode;

/**
 *  用户类型
 */
@property (copy, nonatomic) NSString *roleCode;


/**
 *  用户密码
 */
@property (copy, nonatomic) NSString *userPassword;

/**
 *  用户头像
 */
@property (retain, nonatomic) NSString *headImage;

/**
 *  用户名
 */
@property (copy, nonatomic) NSString *userName;

/**
 *  设备数量
 */
@property(nonatomic,strong) NSString* deviceCount;


+ (instancetype) loginModelWithDic:(NSDictionary *)dic;
- (instancetype) initWithDic:(NSDictionary *)dic;

@end
