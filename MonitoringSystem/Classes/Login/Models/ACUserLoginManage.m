//
//  ACUserLoginManage.m
//  MonitoringSystem
//  Created by JiaKang.Zhong on 16/4/26.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
#import "ACUserLoginManage.h"

#define KJ_COMPANY (@"ac-cNo") // 公司名
#define KJ_ACCOUNT (@"ac-userCode") // 账号
#define KJ_USERTYPE (@"ac-roleCode") // 用户类型
#define KJ_AVATAR (@"ac-headImage") // 头像
#define KJ_NAME (@"ac-userName") // 用户类型
#define KJ_PASSWORD (@"ac-userPassword") // 用户密码
#define KJ_DEVICE_COUTN (@"ac-deviceCount") // 用户所拥有的设备数量


@implementation ACUserLoginManage

// 获取单例对象

SINGLETON_M(shareInstance)


// UserDefault获取value
#define UD_GETVALUE(KEY, SAVE_KEY, TYPE) _##KEY = [UD TYPE##ForKey:SAVE_KEY];


// 定义一个set方法，set属性同时保存到UserDefault
#define UD_SAVE_FUNC(CAP_KEY, KEY, KEY_TYPE, SAVE_KEY, SAVE_TYPE) - (void)set##CAP_KEY:(KEY_TYPE)value { \
    _##KEY = value; \
    [UD set##SAVE_TYPE:value forKey:SAVE_KEY]; \
    [UD synchronize]; \
} \


/**
 *  在获取实例化对象时，获取到原来信息
 *
 */
- (instancetype)init{
    if (self = [super init]) {
        UD_GETVALUE(cNo, KJ_COMPANY, string);
        UD_GETVALUE(userCode, KJ_ACCOUNT, string);
        UD_GETVALUE(roleCode, KJ_USERTYPE, string);
        UD_GETVALUE(headImage, KJ_AVATAR, string);
        UD_GETVALUE(userName, KJ_NAME, string);
        UD_GETVALUE(deviceCount, KJ_DEVICE_COUTN, string);
        UD_GETVALUE(userPassword, KJ_PASSWORD, string);
    }
    return self;
}


/**
 *  封装对象方法，如果需要设置值，就采用 xxxx.userName = @"test" 即可
 *
 */
UD_SAVE_FUNC(CNo, cNo, NSString*, KJ_COMPANY, Object)
UD_SAVE_FUNC(UserCode, userCode, NSString*, KJ_ACCOUNT, Object)
UD_SAVE_FUNC(RoleCode, roleCode, NSString*, KJ_USERTYPE, Object)
UD_SAVE_FUNC(HeadImage, headImage, NSString*, KJ_AVATAR, Object)
UD_SAVE_FUNC(UserName, userName, NSString*, KJ_NAME, Object)
UD_SAVE_FUNC(DeviceCount, deviceCount, NSString*, KJ_DEVICE_COUTN, Object)
UD_SAVE_FUNC(UserPassword, userPassword, NSString*, KJ_PASSWORD, Object)

+ (instancetype)loginModelWithDic:(NSDictionary *)dic{
    return [[self alloc] initWithDic:dic];
}


- (instancetype)initWithDic:(NSDictionary *)dic{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
     
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}
@end

