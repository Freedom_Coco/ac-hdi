//
//  Created by lbxia on 15/10/21.
//  Copyright © 2015年 lbxia. All rights reserved.
//

#import "SubLBXScanViewController.h"
#import "LBXScanResult.h"
#import "LBXScanWrapper.h"
#import "ACQRResultController.h"
#import "NetRequestClass.h"
#import "UtilToolsClass.h"
#import "ACQRDeviceModel.h"
@interface SubLBXScanViewController ()

@end

@implementation SubLBXScanViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
#pragma mark 创建二维码扫描样式类型
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    self.style = style;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.title = NSLocalizedString(@"二维码扫描", nil);
    // 返回按钮   maskLayerGuid1 == 1
    if (![[UD objectForKey:@"maskLayerGuid1-1"] intValue]) { // true隐藏，false 显示
        UIBarButtonItem* backIetm = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back@3x"] style:UIBarButtonItemStylePlain target:self action:@selector(backItemClick)];
        self.navigationItem.leftBarButtonItem = backIetm;
        self.navigationItem.hidesBackButton = YES;
    }else{
        self.navigationItem.hidesBackButton = YES;
    }
}


-(void)backItemClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
            // 页面布局
         [self drawBottomItems];
         [self drawTitle];
         [self.view bringSubviewToFront:_topTitle];
}


- (void)drawTitle
{
    if (!_topTitle)
    {
        self.topTitle = [[UILabel alloc]init];
      
        _topTitle.frame = CGRectMake(SCREEN_WIDTH * 0.1  , 60, SCREEN_WIDTH * 0.8, 60);
        
        //3.5inch iphone
        if ([UIScreen mainScreen].bounds.size.height <= 568 )
        {
            _topTitle.center = CGPointMake(CGRectGetWidth(self.view.frame)/2, 38);
            _topTitle.font = [UIFont systemFontOfSize:14];
        }
        _topTitle.textAlignment = NSTextAlignmentCenter;
        _topTitle.numberOfLines = 0;
        _topTitle.text = NSLocalizedString(@"扫描二维码", nil);
        _topTitle.textColor = [UIColor whiteColor];
        [self.view addSubview:_topTitle];
    }
}


#pragma mark 页面布局
- (void)drawBottomItems
{
    if (_bottomItemsView) {
        
        return;
    }
    
    self.bottomItemsView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.view.frame)-164,CGRectGetWidth(self.view.frame), 100)];
    _bottomItemsView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self.view addSubview:_bottomItemsView];
    
    CGSize size = CGSizeMake(65, 87);
    /**
        打开开灯
     */
    self.btnFlash = [[UIButton alloc] init];
    _btnFlash.bounds = CGRectMake(0, 0, size.width, size.height);
    _btnFlash.center = CGPointMake(CGRectGetWidth(_bottomItemsView.frame)/2, CGRectGetHeight(_bottomItemsView.frame)/2);
     [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_flash_nor"] forState:UIControlStateNormal];
    [_btnFlash addTarget:self action:@selector(openOrCloseFlash) forControlEvents:UIControlEventTouchUpInside];

   [_bottomItemsView addSubview:_btnFlash];
    
}


- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    
    if (array.count < 1){
        [self popAlertMsgWithScanResult:nil];
        return;
    }
    
    //经测试，可以同时识别2个二维码，不能同时识别二维码和条形码
    for (LBXScanResult *sresult in array) {
        
        NSLog(@"scanResult:%@",sresult.strScanned);
    }
    
    LBXScanResult *scanResult = array[0];
    NSString*strResult = scanResult.strScanned;
    self.scanImage = scanResult.imgScanned;
    if (!strResult) {
        [self popAlertMsgWithScanResult:nil];
        return;
    }
    //震动提醒
    [LBXScanWrapper systemVibrate];
    //声音提醒
//    [LBXScanWrapper systemSound];
    // 扫描成功
    [self showNextVCWithScanResult:scanResult];
   
}

- (void)popAlertMsgWithScanResult:(NSString*)strResult
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示",nil) message:@"识别失败" preferredStyle:UIAlertControllerStyleAlert];
    __weak __typeof(self) weakSelf = self;
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定",nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [weakSelf reStartDevice];
    }];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark 显示扫描结果
/**
 *  https://itunes.apple.com/us/app/oilklenze/id1007167022?l=zh&ls=1&mt=8&
 
 */
- (void)showNextVCWithScanResult:(LBXScanResult*)strResult
{
    NSLog(@"扫描到的内容 :%@",strResult.strScanned);
    NSString * finalQRCode = @"";
    /**
     *   防止随便扫码
     */
    if ([strResult.strScanned rangeOfString:@"code="].length == 0) {
        [UtilToolsClass addDisapperAlert:@"" withMessage:@"无法识别二维码的内容"];
          [self reseverQR];
        return;
    }
   NSRange targetRange   =  [strResult.strScanned rangeOfString:@"code="];
    finalQRCode =  [strResult.strScanned substringWithRange:NSMakeRange(targetRange.location + targetRange.length, strResult.strScanned.length -(targetRange.location + targetRange.length))];
    NSLog(@"设备Code : %@",finalQRCode);
    __weak typeof(self) weakSelf = self;
    [[UtilToolsClass getUtilTools] addDoLoading];
    // 请求服务器返回数据
    [NetRequestClass requestWithUrlForDeviceDetail:self andUrl:[REQUESTHEADER stringByAppendingString:@"deviceList/querDetailList.asp"] andParam:@{@"i_imei":finalQRCode} success:^(ACQRDeviceModel * result) {
         [[UtilToolsClass getUtilTools] removeDoLoading];
        if (result) {
            ACQRResultController *qrResult = [[ACQRResultController alloc] initWithModel:result];
            qrResult.enterWithQR = YES;
            [weakSelf.navigationController pushViewController:qrResult animated:YES];
        }

    } failure:^(NSString *failure) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
        [UtilToolsClass addDisapperAlert:@"" withMessage:failure];
         [self reseverQR];
    }];
    
}


- (void) reseverQR{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.style = nil;
        [self.navigationController popViewControllerAnimated:YES];
    });

}

    
#pragma mark - 开关闪光灯
- (void)openOrCloseFlash
{
    [super openOrCloseFlash];
    if (self.isOpenFlash)
    {
        [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_scan_off"] forState:UIControlStateNormal];
    }
    else
        [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_flash_nor"] forState:UIControlStateNormal];
}



- (void)dealloc{
    NSLog(@"释放元素");
}

@end
