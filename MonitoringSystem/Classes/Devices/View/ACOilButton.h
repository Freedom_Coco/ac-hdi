//
//  ACOilButton.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/10/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACOilButton : UIButton

/**
 *  油量条背景
 */
- (void)setOilBGImage:(UIImage *)oilBGImage BringOilValeAbo:(NSString *)oilValue;

- (void)removeOilBG;
@end
