//
//  ACOilButton.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/10/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import "ACOilButton.h"
@interface ACOilButton()
@property(nonatomic,weak) UIImageView * tImageView;
@property(nonatomic,weak) UILabel * tOilValeLable;
@end

@implementation ACOilButton
- (void)awakeFromNib{
    [super awakeFromNib];
    // 背景
    [self setBackgroundImage:[UIImage imageNamed:@"oil_beijing"] forState:UIControlStateNormal];
    // 禁用触摸
    self.userInteractionEnabled = NO;
    // 圆角处理
    self.layer.cornerRadius = 4;
    self.layer.masksToBounds = YES;
}

- (void)setOilBGImage:(UIImage *)oilBGImage BringOilValeAbo:(NSString *)oilValue{
   
    //油量条
    UIImageView * imageView = [[UIImageView alloc] initWithImage:oilBGImage];
    imageView.backgroundColor = [UIColor redColor];
    [self.tImageView sizeToFit];
    [self addSubview:imageView];
    self.tImageView = imageView;
    [self bringSubviewToFront:self.titleLabel];
}



- (void)removeOilBG{
    [self.tImageView removeFromSuperview];
    
}
@end
