//
//  ACDeviceTitleView.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ACSearchDeviceForKeyWordDelegete <NSObject>
@required
- (void) searchDeviceForKeyWord:(NSString *)keyWord show:(BOOL) showDeviceState;
@end


@interface ACDeviceTitleView : UIView

- (instancetype) initWithFrameWithSelf:(UIViewController *)controller;

/**
 *   委托回调
 */
@property (nonatomic,assign) id<ACSearchDeviceForKeyWordDelegete> delegate;



@end
