//
//  ACDeviceTitleView.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACDeviceTitleView.h"
#import "LBXScanViewController.h"
#import "SubLBXScanViewController.h"
#import "ACQRResultController.h"
#import "NetRequestClass.h"
#import "UtilToolsClass.h"
#import "ACSettingController.h"
@interface ACDeviceTitleView ()<UITextViewDelegate>{
    UIViewController * _controller;
    BOOL isClearn;// 是否清除
}
@end

@implementation ACDeviceTitleView
- (instancetype)initWithFrameWithSelf:(UIViewController *)controller{
    
    if (self = [super initWithFrame:ACStatusHieght(controller)]) {
        [self setUpChildView];
        _controller = controller;
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUpChildView];
    }
    return self;
}


- (void) setUpChildView{
    
     isClearn = true;
    CGFloat titleViewW = self.frame.size.width;
    CGFloat richSearchX = 2;
    CGFloat richSearchY = richSearchX ;
    CGFloat enterSettingW = 48;
    // 二维码扫描器
    UIButton * richSearch = [UIButton buttonWithType:UIButtonTypeCustom];
    [richSearch setImage:[UIImage imageNamed:@"RichScan@3x"] forState:UIControlStateNormal];
    richSearch.frame = CGRectMake(richSearchX, richSearchY + 5 ,enterSettingW + 4,enterSettingW + 4);
    [richSearch sizeToFit];
    [self addSubview:richSearch];
    [richSearch addTarget:self action:@selector(showQRCode) forControlEvents:UIControlEventTouchUpInside];
    
    // 放大镜
    UIImageView * searchView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"device_search@3x"]];
    searchView.center = CGPointMake(CGRectGetMaxX(searchView.frame) + 20, CGRectGetMidY(richSearch.frame));
    [self addSubview:searchView];
    
    // 输入框
    UITextView * textView = [[UITextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchView.frame),CGRectGetMinY(searchView.frame) - 10, titleViewW - CGRectGetMaxX(searchView.frame) - enterSettingW - 20,  40)];
    textView.backgroundColor = [UIColor clearColor];
     textView.delegate = self;
    textView.text = NSLocalizedString(@"搜索", nil);
    textView.textColor = [UIColor whiteColor];
    textView.returnKeyType = UIReturnKeySearch;
    textView.scrollEnabled = NO;
    textView.font = [UIFont systemFontOfSize:14];
    textView.textContainer.lineFragmentPadding = 0.0f;
    textView.textContainerInset = UIEdgeInsetsMake(20, 0, 0, 0);
    [self addSubview:textView];

    // 底部线条
    UIView * bottomLine = [[UIView alloc] initWithFrame:CGRectMake(searchView.frame.origin.x + 10, CGRectGetMaxY(searchView.frame) - richSearchX , titleViewW - searchView.size.width - richSearch.size.width - enterSettingW, 1)];
    bottomLine.backgroundColor = [UIColor whiteColor];
    bottomLine.alpha = 0.5;
    [self addSubview:bottomLine];
    
    // 进入设置界面
    UIButton * enterSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [enterSetting setImage:[UIImage imageNamed:@"icon-listmore"] forState:UIControlStateNormal];
    enterSetting.frame = CGRectMake(titleViewW -  60 , 0,enterSettingW,enterSettingW);
    [self addSubview:enterSetting];
    [enterSetting addTarget:self action:@selector(enterSettingCallBack) forControlEvents:UIControlEventTouchUpInside];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if (isClearn) {
         textView.text = @"";
        isClearn = false;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length > 14) {
        textView.text = [textView.text substringToIndex:14];
    }
    if ([textView.text isEqualToString:@""]) {
        if ([self.delegate respondsToSelector:@selector(searchDeviceForKeyWord:show:)]) {
            [self endEditing:YES];
            [self.delegate searchDeviceForKeyWord:textView.text show:true];
        }
    }
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        if ([self.delegate respondsToSelector:@selector(searchDeviceForKeyWord:show:)]) {
              [self endEditing:YES];
            [self.delegate searchDeviceForKeyWord:textView.text show:false];
        }
       
    }
    return YES;
}


/**
 *    显示二维码
 */
- (void) showQRCode{
    SubLBXScanViewController *vc = [[SubLBXScanViewController alloc] init];
    [_controller.navigationController pushViewController:vc animated:YES];
}


/**
 *    进入下一级界面
 */
- (void) enterSettingCallBack{
    ACSettingController * settingController = [[ACSettingController alloc] init];
    [_controller.navigationController pushViewController:settingController animated:YES];
}


- (UIViewController *)getCurrentVC{
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UINavigationController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

@end
