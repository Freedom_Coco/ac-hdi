//
//  ACDeviceListModelCenter.m
//  MonitoringSystem
//  Created by JiaKang.Zhong on 16/5/17.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
#import "ACDeviceListModelCenter.h"
#import "ACDeviceListModel.h"
#import "UtilToolsClass.h"
@implementation ACDeviceListModelCenter

- (instancetype)initWithDic:(NSDictionary *)dic{
    if (self = [super init]) {
        self.allDevice = [NSMutableArray array];
        self.normarlDevice = [NSMutableArray array];
        self.noWorkDevice = [NSMutableArray array];
        self.breakDownDevice = [NSMutableArray array];
        self.breakDownAndNoWorkDevice = [NSMutableArray array];
    }
    return self;
}



+ (instancetype)deveiceModelWithDic:(NSDictionary *)dic{
    return [[self alloc] initWithDic:dic];
}



- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
    
}

- (void) converModelDataWithArray:(NSDictionary *) arrDic withbtnState:(NSInteger)state withIsReFresh:(BOOL ) isReFresh{
    
    // 解析数据-并加入到对应的状态的数组中
    NSMutableArray * tempAllDeviceArr = [NSMutableArray array];
    NSMutableArray * tempNormalArr = [NSMutableArray array];
    NSMutableArray * tempnoWorkArr = [NSMutableArray array];
    NSMutableArray * tempbreakDownArr = [NSMutableArray array];
    NSMutableArray * tempBreakAndNoWorkArr = [NSMutableArray array];
    
    NSArray * deviceList = arrDic[@"deviceList"];
    if(deviceList.count > 0){
        for ( NSDictionary * tempModel in arrDic[@"deviceList"]) {
            ACDeviceListModel * listModel = [ACDeviceListModel listModelWithDic:tempModel];
            if (![[NSString stringWithFormat:@"%@",tempModel[@"i_state"]] isEqualToString:@""]) {
                // 所有的设备
                [tempAllDeviceArr addObject:listModel];
                // 如下的设备是按照状态来取
                NSInteger deviceState = [tempModel[@"i_state"] integerValue];
                if (deviceState == 1 || deviceState == 2) {
                    [tempNormalArr addObject:listModel];
                }
                if (deviceState == 3 ){
                    [tempBreakAndNoWorkArr addObject:listModel];
                }
                if (deviceState == 4 ){
                    [tempBreakAndNoWorkArr addObject:listModel];
                }
            }
        }

    }else{
        if ([arrDic[@"allDevice"] intValue] == 0) {
                [UtilToolsClass addDisapperAlert:@"" withMessage:@"当前用户无绑定设备"];
        }else{
            [UtilToolsClass addDisapperAlert:@"" withMessage:@"无数据"];
        }
        
    }
       if (isReFresh) {
        self.normalDeviceCount = [arrDic[@"normalDevice"] intValue];
        self.allDeviceCount = [arrDic[@"allDevice"] intValue];
        self.breakDownDeviceCount = [arrDic[@"faultDevice"] intValue];
        // 刷新----- 覆盖原来的数据源的数组
        if(state == 0){
            self.allDevice = tempAllDeviceArr;
            self.normarlDevice = tempNormalArr;
            self.noWorkDevice = tempnoWorkArr;
            self.breakDownDevice = tempbreakDownArr;
            self.breakDownAndNoWorkDevice = tempBreakAndNoWorkArr;
        }
        if (state == 1) {
            self.normarlDevice = tempNormalArr;
        }
        if (state == 2) {
            self.noWorkDevice = tempnoWorkArr;
            self.breakDownDevice = tempbreakDownArr;
            self.breakDownAndNoWorkDevice  = tempBreakAndNoWorkArr;
        }
    }else{
        // 上拉加载---- 只需要加到原来的数据中就好了
        if(state == 0){
            [self.allDevice addObjectsFromArray:tempAllDeviceArr];
            [self.normarlDevice addObjectsFromArray:tempNormalArr];
            [self.noWorkDevice addObjectsFromArray:tempnoWorkArr];
            [self.breakDownDevice addObjectsFromArray:tempbreakDownArr];
        }
        if (state == 1) {
            [self.normarlDevice addObjectsFromArray:tempNormalArr];
        }
        if (state == 2) {
            [self.normarlDevice addObjectsFromArray:tempNormalArr];
            [self.noWorkDevice addObjectsFromArray:tempnoWorkArr];
            [self.breakDownDevice addObjectsFromArray:tempbreakDownArr];
        }
    }
}



@end
