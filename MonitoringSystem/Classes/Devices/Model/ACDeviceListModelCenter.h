//
//  ACDeviceListModelCenter.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/17.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACDataEntity.h"

@interface ACDeviceListModelCenter : ACDataEntity


@property(nonatomic,assign) int allDeviceCount;
@property(nonatomic,assign) int normalDeviceCount;
@property(nonatomic,assign) int breakDownDeviceCount;
/**
 *  所有的设备
 */
@property (nonatomic,strong) NSMutableArray * allDevice;

/**
 *  正常的设备
 */
@property(nonatomic,strong) NSMutableArray * normarlDevice;

/**
 *  非作业的设备
 */
@property(nonatomic,strong) NSMutableArray * noWorkDevice;

/**
 *  马达报警设备
 */
@property(nonatomic,strong) NSMutableArray * breakDownDevice;

@property (nonatomic,strong) NSMutableArray * breakDownAndNoWorkDevice;

- (instancetype) initWithDic:(NSDictionary *)dic;

+ (instancetype) deveiceModelWithDic:(NSDictionary *)dic;


- (void) converModelDataWithArray:(NSDictionary *) arrDic withbtnState:(NSInteger)state withIsReFresh:(BOOL ) isReFresh;

@end
