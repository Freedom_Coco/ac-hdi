//
//  ACDeviceController.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/25.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
/**
 *      设备显示界面，控制器
 */


#import <UIKit/UIKit.h>

@interface ACDeviceController : UIViewController


@end
