//
//  ACDeviceController.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/25.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACDeviceController.h"
#import "ACDataSource.h"
#import "ACDeviceTitleView.h"
#import "NetRequestClass.h"
#import "UtilToolsClass.h"

#import "ACDeviceListModelCenter.h"
#import "ACDeviceListModel.h"
#import <IQKeyboardManager.h>
#import "MJRefreshComponent.h"
#import <MJRefreshBackNormalFooter.h>
#import "MJRefreshFooter.h"
#import "AddressView.h"
#import "ACDeviceInfoController.h"
#import <ALBBSDK/ALBBSDK.h>
#import <ALBBPush/CloudPushSDK.h>
#import "ACQRResultController.h"
#import "MapGPSViewController.h"
#import "YLZHoledView.h"
#import "SubLBXScanViewController.h"
#import "ACUpdateVersionController.h"
@interface ACDeviceController ()<UITextFieldDelegate,ACSearchDeviceForKeyWordDelegete,UIScrollViewDelegate,YLZHoledViewDelegate>{
    int currPage;// 当前业的数据
    CGFloat topPosition;// 顶部的位置
    UIButton * CurrentSelectBtn;// 状态选中按钮
    BOOL isRefreshData; //是否是刷新数据
   
    
}
@property (nonatomic,strong) NSMutableArray * dataMutableArr; // 数据接收
@property (nonatomic,strong) ACDataSource * acDataSource; // tableView的委托对象
@property (nonatomic,strong) ACDeviceListModelCenter * modelCenter;// 模型对象
@property (nonatomic,strong) UITableView * deviceTableView;
@property(nonatomic,weak) UIButton * btnAll;// 所有
@property(nonatomic,weak) UIButton * btnNormal;// 所有
@property(nonatomic,weak) UIButton * btnBreakDown;// 所有
@property (nonatomic, strong) YLZHoledView *holedView; //遮罩层
@property(nonatomic,weak) UIImageView * tGuidView_1;
@end

@implementation ACDeviceController

- (UITableView *)deviceTableView{
    if (!_deviceTableView) {
      _deviceTableView =  [[UITableView alloc] initWithFrame:CGRectMake(0, topPosition , SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];//- topPosition * 2 - ACStatusHieght(self).size.height * 2)
        _deviceTableView.backgroundColor = ACCommentColor;
      _deviceTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
     [_deviceTableView registerNib:[UINib nibWithNibName:@"ACDeviceViewCell" bundle:nil] forCellReuseIdentifier:@"acDeviceViewCell"];
      [self.view addSubview:_deviceTableView];
    }
    return _deviceTableView;
}
- (NSMutableArray *)dataMutableArr{
    if (!_dataMutableArr) {
        _dataMutableArr = [NSMutableArray array];
    }
    return _dataMutableArr;
}

- (ACDeviceListModelCenter *)modelCenter{
    if (!_modelCenter) {
        _modelCenter = [[ACDeviceListModelCenter alloc] init];
    }
    return _modelCenter;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ACCommentColor;
   
    /*    设置导航条的头部视图  */
    [self setUpNaviagtionTitleView];
    /*    配置UI界面    */
    [self setUpUI];
    /*    添加刷新控件  */
    [self setUpRefreshView];
    /*    检查更新 --- 在无引导的情况下，避免产生冲突,随机数大于一定的几率才进行检查更新 */
    if ([[UD objectForKey:@"maskLayerGuid1"] intValue] && arc4random() % 10 > 4) {
        [self checkUpdate];
    }
    /*    解绑设备  */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self unBindAccountForPush];
    });
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
    self.edgesForExtendedLayout = UIRectEdgeAll;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsCompact];
    self.navigationController.navigationBar.translucent = NO;
    // 马上进入刷新状态
    [self stopRefreshView];
    [self.deviceTableView.mj_header beginRefreshing];
    currPage = 1;
   
}

-(void)viewDidAppear:(BOOL)animated{
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}


#pragma mark
- (void) loadMaskView{
    //添加遮罩指示
    self.holedView = [[YLZHoledView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.holedView.holeViewDelegate = self;
    // 圆形
    [self.holedView initWithMaskLayerWithObj:CGRectMake(0, 0, 80, 80) layerRadius:30 fingerRect:CGPointMake(60,80) holdViewTag:0];
    UIImageView * guidLabelView  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guiderweima"]];
    [guidLabelView sizeToFit];
    guidLabelView.layer.anchorPoint = CGPointMake(0,0);
    guidLabelView.center = CGPointMake(20, 120);
    [self.holedView addSubview:guidLabelView];
    [[UIApplication sharedApplication].keyWindow addSubview:self.holedView];
}


- (void)holedView:(YLZHoledView *)holedView didSelectHoleAtIndex:(NSUInteger)index{
    if(index == 0){
        [UD setObject:@(1) forKey:@"maskLayerGuid1-1"]; // 开启设备列表引导
        [UD synchronize];
        [self.holedView removeFromSuperview];
        SubLBXScanViewController *vc = [[SubLBXScanViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void) initGuidView{
    UIImageView * guidView_1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guiderwsblb"]];
    guidView_1.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [[UIApplication sharedApplication].keyWindow addSubview:guidView_1];
    self.tGuidView_1 = guidView_1;
    UIButton * nextBtn = [[UIButton alloc] init];
    [nextBtn setImage:[UIImage imageNamed:@"Guiderwxiayibu"] forState:UIControlStateNormal];
    [nextBtn sizeToFit];
    nextBtn.tag = 0;
    nextBtn.center = CGPointMake(SCREEN_WIDTH * 0.5, SCREEN_HEIGHT * 0.9);
    [[UIApplication sharedApplication].keyWindow addSubview:nextBtn];
    [nextBtn addTarget:self action:@selector(loadingDeviceGuid_2:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void) loadingDeviceGuid_2:(UIButton *)sender{
    if (sender.tag == 0) {
        self.tGuidView_1.image = [UIImage imageNamed:@"Guiderwsblb2"];
        [sender setImage:[UIImage imageNamed:@"Guiderwwozhidaole"] forState:UIControlStateNormal];
        sender.tag = 1;
    }else if (sender.tag == 1){
        [UD setObject:@(0) forKey:@"maskLayerGuid1-1"];
        [UD setObject:@(1) forKey:@"maskLayerGuid2"];
        [self.tGuidView_1 removeFromSuperview];
        [sender removeFromSuperview];
       
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}


#pragma mark 解绑操作
- (void) unBindAccountForPush{
    [CloudPushSDK unbindAccount:[ACUserLoginManage shareInstance].userCode withCallback:^(BOOL success) {
        if (success) {
            NSLog(@"解绑成功");
            [self startBindPushAccount];
        }else{
            NSLog(@"解绑失败");
        }
    }];
}


// 绑定推送账号
- (void)startBindPushAccount {
    [CloudPushSDK bindAccount:[ACUserLoginManage shareInstance].userCode withCallback:^(BOOL success) {
        if(success) {
            NSLog(@"-->> 推送账号绑定成功");
        } else {
            NSLog(@"-->> 推送账号绑定失败");
            // 1.5秒后继续重试
            [self performSelector:@selector(startBindPushAccount) withObject:nil afterDelay:1.5];
        }
    }];
}

- (void)setUpRefreshView{
    // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的loadNewData方法）
    __weak typeof(self) weakSelf = self;
   self.deviceTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
       [weakSelf loadNewData];
    }];
    self.deviceTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadOldData];
    }];
}

/**
 *  下拉刷新
 */
- (void)loadNewData{
    currPage = 1;
    isRefreshData = true;
    /*    刷新数据  */
    [self getDeviceListData:@"" andAscendingOrder:@"" andOrderName:@"" andCurrent_page:1 andbtnState:CurrentSelectBtn.tag];
}


/**
 *  上拉加载新的数据
 */
- (void)loadOldData{
    /*    刷新数据  */
    isRefreshData = false;
    [self getDeviceListData:@"" andAscendingOrder:@"" andOrderName:@"" andCurrent_page:currPage andbtnState:CurrentSelectBtn.tag];
}

#pragma mark 配置UI界面
- (void) setUpUI{
    self.view.backgroundColor = ACCommentColor; // 背景
    UIView * speaceLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  SCREEN_WIDTH, 1)];
    speaceLineView.backgroundColor = [UIColor whiteColor];
    speaceLineView.alpha = 0.3;
    [self.view addSubview:speaceLineView];
    
    // 全部按钮
    CGFloat btnX = 5;
    CGFloat btnW = (SCREEN_WIDTH - btnX * 2 )/ 3 ;
    CGFloat btnH = 30;
    CGFloat btnY = CGRectGetMaxY(speaceLineView.frame)+ 10;
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"menu-l-off"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"menu-l-on"] forState:UIControlStateSelected];
    [leftBtn setTitle:@"全部(0)" forState:UIControlStateNormal];
    leftBtn.titleLabel.textColor = [UIColor whiteColor];
    leftBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    leftBtn.frame = CGRectMake(btnX, btnY, btnW, btnH);
    leftBtn.tag = 0;
    [self.view addSubview:leftBtn];
    [leftBtn addTarget:self action:@selector(devicebtnStateViewCallBakc:) forControlEvents:UIControlEventTouchDown];
    leftBtn.selected = YES;
    self.btnAll = leftBtn;
    CurrentSelectBtn = leftBtn;
    
    // 正常按钮
    UIButton * midBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [midBtn setBackgroundImage:[UIImage imageNamed:@"menu-c-off"] forState:UIControlStateNormal];
    [midBtn setBackgroundImage:[UIImage imageNamed:@"menu-c-on"] forState:UIControlStateSelected];
    [midBtn setTitle:@"正常(0)" forState:UIControlStateNormal];
    midBtn.titleLabel.textColor = [UIColor whiteColor];
    midBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    CGFloat midBtnX = CGRectGetMaxX(leftBtn.frame);
    midBtn.frame = CGRectMake(midBtnX, btnY, btnW, btnH);
    midBtn.tag = 1;
    [self.view addSubview:midBtn];
    self.btnNormal = midBtn;
    [midBtn addTarget:self action:@selector(devicebtnStateViewCallBakc:) forControlEvents:UIControlEventTouchDown];
    
    // 故障按钮
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"menu-r-off"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"menu-r-on"] forState:UIControlStateSelected];
    [rightBtn setTitle:@"故障(0)" forState:UIControlStateNormal];
    rightBtn.titleLabel.textColor = [UIColor whiteColor];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    CGFloat rightBtnX = CGRectGetMaxX(midBtn.frame);
    rightBtn.frame = CGRectMake(rightBtnX, btnY, btnW, btnH);
    rightBtn.tag = 2;
    [self.view addSubview:rightBtn];
    self.btnBreakDown = rightBtn;
    [rightBtn addTarget:self action:@selector(devicebtnStateViewCallBakc:) forControlEvents:UIControlEventTouchDown];
    topPosition = CGRectGetMaxY(rightBtn.frame);
  
}


#pragma mark 指定状态请求
- (void)devicebtnStateViewCallBakc:(UIButton *)sender{
    CurrentSelectBtn.selected = NO;
    sender.selected = YES;
    CurrentSelectBtn = sender;
    /*    刷新数据  */
    [self stopRefreshView];
    [self.deviceTableView.mj_header beginRefreshing];
}


/**
 *   配置头部视图
 */
- (void) setUpNaviagtionTitleView{
    ACDeviceTitleView * titleView = [[ACDeviceTitleView alloc] initWithFrameWithSelf:self];
    titleView.delegate = self;
    self.navigationItem.titleView = titleView;
}


#pragma mark 获取设备列表数据 ----------- 所有数据 首次进入，数据分栏展示，回调这个方法
/**
 *  此处的result 返回过来都是有值的，没有值的情况都在内部进行的处理，并返回了不一样的结果值
 *
 */
- (void) getDeviceListData:(NSString *)keyWord andAscendingOrder:(NSString *)ascendingOrder andOrderName:(NSString *)orderName andCurrent_page:(NSInteger)current_page andbtnState:(NSInteger)btnState{
    
    __weak typeof(self )weakSelf = self;
    [NetRequestClass requestWithUrlForDviceList:self andUrl:[REQUESTHEADER stringByAppendingString:@"deviceList/deviceList.asp"] andParam:keyWord andAscendingOrder:ascendingOrder andOrderName:orderName andPage:current_page andState:btnState success:^(NSDictionary *result) {
        [weakSelf requestTrantsitionData:result andbtnState:(NSInteger)btnState];
       
    } failure:^(NSString *failure) {
        [weakSelf stopRefreshView];
        [UtilToolsClass addDisapperAlert:@"" withMessage:failure];
    }];
}


/**
 *   返回数据的处理
 *
 */
- (void) requestTrantsitionData:(NSDictionary *)arr andbtnState:(NSInteger)btnState{
    
    // 移除刷新视图
    [self stopRefreshView];
    // 将数据转换为模型数据
    [self.modelCenter converModelDataWithArray:arr withbtnState:btnState withIsReFresh:isRefreshData];
    if (self.modelCenter.allDevice.count <= 0) { // 无任何数据信息，开启引导
        NSInteger isNeedMask = [[UD objectForKey:@"maskLayerGuid1"] intValue]; // 默认值为0
        NSInteger firstEnterApp = [[UD objectForKey:@"FIRSTENTER"] intValue];
        if(!isNeedMask && !firstEnterApp){ // 解绑设备，会刷新此方法，因此还要做一个判断。不然引导会重现
             [self loadMaskView];
        }
    }else{
        // 有设备信息--- 开始引导设备列表
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([[UD objectForKey:@"maskLayerGuid1-1"] intValue]) {
                [self initGuidView];
            }
        });
        [UD setObject:@(1) forKey:@"FIRSTENTER"];// 在引导二维码期间，如果给强杀了，那么接下来进入也是继续引导
    }
    // 上拉列表，获取更多数据，如果有数据返回了，才将下标加1
     currPage += 1;
    if (btnState == 0) {
        _acDataSource = [ACDataSource dataSourceWithEntity:(ACDataEntity *)self.modelCenter andWithDeviceState:btnState];
        //设置tableView的带来为dataSource、delagete对象
        _acDataSource.superController = self;
        self.deviceTableView.delegate = _acDataSource;
        self.deviceTableView.dataSource = _acDataSource;
        //3.设置block回调
        __weak typeof (self) weakSelf = self;
        ACTableViewCellClick cellSelectedBlock = ^(NSInteger obj,NSString * imei){
            // cell点击事件
            [weakSelf tableViewDidSelectCellClick:obj imei:imei];
        };
        _acDataSource.didSelectCellClick = cellSelectedBlock;
        // 设置设备的数量关系
        [self.btnAll setTitle:[NSString stringWithFormat:@"所有(%d)",self.modelCenter.allDeviceCount] forState:UIControlStateNormal];
        [self.btnNormal setTitle:[NSString stringWithFormat:@"正常(%d)",self.modelCenter.normalDeviceCount] forState:UIControlStateNormal];
        [self.btnBreakDown setTitle:[NSString stringWithFormat:@"故障(%d)",self.modelCenter.breakDownDeviceCount] forState:UIControlStateNormal];
        self.acDataSource.dataModelArray = self.modelCenter.allDevice;
    }else if(btnState == 1){
        [self.btnNormal setTitle:[NSString stringWithFormat:@"正常(%d)",(int)self.modelCenter.normalDeviceCount] forState:UIControlStateNormal];
        [self.btnAll setTitle:[NSString stringWithFormat:@"所有(%d)",self.modelCenter.allDeviceCount] forState:UIControlStateNormal];
        self.acDataSource.dataModelArray = self.modelCenter.normarlDevice;
    }else if (btnState == 2){
        
        [self.btnBreakDown setTitle:[NSString stringWithFormat:@"故障(%d)",self.modelCenter.breakDownDeviceCount] forState:UIControlStateNormal];
        [self.btnAll setTitle:[NSString stringWithFormat:@"所有(%d)",self.modelCenter.allDeviceCount] forState:UIControlStateNormal];
        self.acDataSource.dataModelArray = self.modelCenter.breakDownAndNoWorkDevice;
    }
    [self.deviceTableView reloadData];
    
}

/**
 *  移除刷新的视图
 */
- (void) stopRefreshView{
    if ([self.deviceTableView.mj_header isRefreshing]) {
        [self.deviceTableView.mj_header endRefreshing];
    }
    if ([self.deviceTableView.mj_footer isRefreshing]) {
        [self.deviceTableView.mj_footer endRefreshing];
    }
}


#pragma mark ******************     block回调cell中的选中     ******************
/**
 *  obj 包含了组数，行数
 *
    imei
 */
-(void) tableViewDidSelectCellClick :(NSInteger) obj imei:(NSString *)imei{

    if (obj == 0) {
        // 设备信息
        ACQRResultController * resultController = [[ACQRResultController alloc] init];
        resultController.i_imei = imei;
        resultController.enterWithQR = false;
        [self.navigationController pushViewController:resultController animated:YES];
    }else if(obj == 1){
        // GPS
        MapGPSViewController * mapViewController = [[MapGPSViewController alloc] init];
        mapViewController.device_IMEI = imei;
        [self.navigationController pushViewController:mapViewController animated:YES];
    }else if(obj == 2){
        // 进入设备详情
        ACDeviceInfoController * infoController = [[ACDeviceInfoController alloc] init];
        infoController.i_imei = imei;
        [self.navigationController pushViewController:infoController animated:YES];
    }else{}
}

#pragma mark ******************     ACSearchDeviceDelegete delegate选中     ******************
- (void) searchDeviceForKeyWord:(NSString *)keyWord show:(BOOL) showDeviceState {
    isRefreshData = true;
    // 需要全局刷新
    [self getDeviceListData:keyWord andAscendingOrder:@"" andOrderName:@"" andCurrent_page:1 andbtnState:CurrentSelectBtn.tag];
    [self showOrHidenDeviceState:showDeviceState];
}

- (void) showOrHidenDeviceState:(BOOL) isShow{
    self.btnAll.hidden = !isShow;
    self.btnNormal.hidden = !isShow;
    self.btnBreakDown.hidden = !isShow;
    if (!isShow) {
        // 上移
        [UIView animateWithDuration:0.5 animations:^{
            self.deviceTableView.transform = CGAffineTransformMakeTranslation(0,  - topPosition + 1);
        }];
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            self.deviceTableView.transform = CGAffineTransformIdentity;
        }];
    }
}


#pragma mark 检查更新
- (void) checkUpdate{
    NSString * appUpdateUrl = [NSString stringWithFormat:@"https://itunes.apple.com/lookup?id=%@",@"1007167022"];
 
    [NetRequestClass requestWithUrlForUpdateApp:appUpdateUrl success:^(NSDictionary *versionInfo) {
        if(!versionInfo){
            return ;
        }
        // 获取到的版本信息
        NSLog(@"info:%@",versionInfo[@"results"]);
        // 获取到当前版本信息
        NSDictionary * versionDic = [versionInfo[@"results"] lastObject];
        NSString * newVersion = versionDic[@"version"];
        NSLog(@"newVersion:%@",versionDic[@"version"]);
      NSString * currVersion  = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey]];
        if (![newVersion isEqualToString:currVersion]) {
              NSLog(@"发起更新请求");
            ACUpdateVersionController * updateVersionController = [[ACUpdateVersionController alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            [[UIApplication sharedApplication].keyWindow addSubview:updateVersionController];
            
        }else{
            NSLog(@"版本一致");
            [MBProgressHUD showSuccess:@"当前是最新版本"];
        }
        
    } failure:^(NSString *failure) {
        // 友好体验，错误信息不处理
        NSLog(@"获取更新失败");
    }];
}


@end
