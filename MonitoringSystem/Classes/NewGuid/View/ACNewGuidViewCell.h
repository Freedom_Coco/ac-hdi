//
//  ACNewGuidViewCell.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/1/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACNewGuidViewCell : UICollectionViewCell


-(void) setUpCollectionViewCellStyle:(NSIndexPath *)indexPath;

-(void) setIndexPath :(NSIndexPath *)indexPath and: (int) count;


@end
