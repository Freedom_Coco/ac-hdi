//
//  ACNewGuidViewCell.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/1/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import "ACNewGuidViewCell.h"

@interface ACNewGuidViewCell ()
@property(nonatomic,weak) UIButton * startButton; // 开始按钮
@property(nonatomic,weak) UIImageView * commontView; // 内容视图
@property(nonatomic,strong) UILabel * commontTopLable; // 显示上方文字
@property(nonatomic,strong) UILabel * commontBottonLable; // 显示上方文字
@property(nonatomic,strong) NSMutableArray * guidTopTextArr;// 引导文字数组
@property(nonatomic,strong) NSMutableArray * guidButtonTextArr;// 引导文字数组
@end
@implementation ACNewGuidViewCell
- (UIButton *)startButton
{
    if (_startButton == nil) {
        UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [startBtn setBackgroundImage:[UIImage imageNamed:@"anniu"] forState:UIControlStateNormal];
        [startBtn setBackgroundImage:[UIImage imageNamed:@"anniu"] forState:UIControlStateFocused];
        [startBtn setTitle:@"立即体验" forState:UIControlStateNormal];
        _startButton = startBtn;
        [startBtn sizeToFit];
        [startBtn addTarget:self action:@selector(start) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:startBtn];
        
    }
    return _startButton;
}

-(UIView *)commontView{
    if(_commontView == nil){
        UIImageView * comView = [[UIImageView alloc] init];
        [self.contentView addSubview:comView];
        _commontView = comView;
        [self.contentView addSubview:comView];
    }
    
    return _commontView;
}

- (UILabel *)commontTopLable{
    if (!_commontTopLable) {
        _commontTopLable = [[UILabel alloc] init];
        _commontTopLable.textAlignment = NSTextAlignmentCenter;
        _commontTopLable.font = [UIFont systemFontOfSize:26];
        _commontTopLable.textColor = [UIColor whiteColor];
        [self.contentView addSubview: _commontTopLable];
    }
    return _commontTopLable;
}


- (UILabel *)commontBottonLable{
    if (!
        _commontBottonLable) {
        UILabel * bLable = [[UILabel alloc] init];
        bLable.textAlignment = NSTextAlignmentCenter;
        bLable.font = [UIFont systemFontOfSize:18];
        bLable.textColor = [UIColor whiteColor];
        _commontBottonLable = bLable;
        [self.contentView addSubview:bLable];
    }
    return _commontBottonLable;
}

- (NSMutableArray *)guidTopTextArr{
    if (!_guidTopTextArr) {
        NSMutableArray * arr  = [NSMutableArray array];
        [arr addObject:@"扫一扫"];
        [arr addObject:@"设备列表"];
        [arr addObject:@"GPS定位"];
        self.guidTopTextArr = arr;
    }
    return _guidTopTextArr;
}

- (NSMutableArray *)guidButtonTextArr{
    if (!_guidButtonTextArr) {
        NSMutableArray * arr  = [NSMutableArray array];
        [arr addObject:@"扫一扫,设备动态瞬间关注   "];
        [arr addObject:@"云服务,大数据,设备详情想看就看"];
        [arr addObject:@"全球GPS定位,设备位置尽在掌控之中"];
        self.guidButtonTextArr = arr;
    }
    return _guidButtonTextArr;
}


-(void)layoutSubviews{
    [super layoutSubviews];
    // 开始按钮
    self.startButton.center = CGPointMake(self.frame.size.width * 0.5, self.frame.size.height * 0.9 - 30);
    // 内容
    self.commontView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    // 标题
    self.commontTopLable.frame = CGRectMake(0,self.frame.size.height * 0.62,self.frame.size.width , 50);
    // 子标题
    self.commontBottonLable.frame = CGRectMake(0, self.frame.size.height * 0.72,self.frame.size.width ,40);
}


-(void)setUpCollectionViewCellStyle:(NSIndexPath *)indexPath{
    NSLog(@"indexPath:%d",(int)indexPath.row);
    self.commontView.image = [UIImage imageNamed:[NSString stringWithFormat:@"yindaoye%d.png",(int)(indexPath.row + 1)]];
    self.commontTopLable.text = self.guidTopTextArr[indexPath.row];
    self.commontBottonLable.text = self.guidButtonTextArr[indexPath.row];
}


-(void)setIndexPath:(NSIndexPath *)indexPath and:(int)count{
    if(indexPath.row == count -1){
        self.startButton.hidden = NO;
    }else{
        self.startButton.hidden = YES;
    }
}


- (void)start{
    dispatch_after(0.2, dispatch_get_main_queue(), ^{
        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        ACApplicationKeyWindow.rootViewController = [storyBoard instantiateInitialViewController];
    });
   
}
@end
